package cz.sajwy.testapp.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import cz.sajwy.testapp.data.DBManager;
import cz.sajwy.testapp.data.model.Den;

public class DenRepo {
    private static final String TABLE = "Den";

    private static final String KEY_ID_DEN = "ID_den";
    private static final String KEY_NAZEV = "Nazev";
    private static final String KEY_ZKRATKA = "Zkratka";

    private static final String[] COLUMNS = {KEY_ID_DEN, KEY_NAZEV, KEY_ZKRATKA};

    public DenRepo() {}

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_ID_DEN  + " INTEGER PRIMARY KEY," +
                    KEY_NAZEV + " TEXT, " +
                    KEY_ZKRATKA + " TEXT " +
                ");";
    }

    public static String createIndex() {
        return "CREATE INDEX ix_nazevDne ON " + TABLE + "(" + KEY_NAZEV + ");"+
                "CREATE INDEX ix_idDne ON " + TABLE + "(" + KEY_ID_DEN + ");";
    }

    public static String insertData(){
        return  "INSERT INTO " + TABLE + " (" + KEY_NAZEV + ", " + KEY_ZKRATKA + ") VALUES " +
                    "('Pondělí', 'PO'), " +
                    "('Úterý', 'ÚT'), " +
                    "('Středa', 'ST'), " +
                    "('Čtvrtek', 'ČT'), " +
                    "('Pátek', 'PÁ'), " +
                    "('Sobota', 'SO'), " +
                    "('Neděle', 'NE'); ";
    }

    public static ArrayList<Den> getDny() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT * FROM " + TABLE + " ORDER BY " + KEY_ID_DEN;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Den> dny = new ArrayList<Den>();

        if (cursor.moveToFirst()) {
            do {
                Den den = new Den(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID_DEN))),
                        cursor.getString(cursor.getColumnIndex(KEY_NAZEV)),cursor.getString(cursor.getColumnIndex(KEY_ZKRATKA)));

                dny.add(den);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return dny;
    }

    public static String[] getNazvyDnuArray() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT " + KEY_NAZEV + " FROM " + TABLE + " ORDER BY " + KEY_ID_DEN;
        Cursor cursor = db.rawQuery(query, null);
        String[] dnyArray = new String[7];

        int i = 0;
        if (cursor.moveToFirst()) {
            do {
                String nazevDne = cursor.getString(cursor.getColumnIndex(KEY_NAZEV));
                dnyArray[i] = nazevDne;
                i++;
            } while (cursor.moveToNext());
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return dnyArray;
    }

    public static Den getDenByID(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        Cursor cursor =
                db.query(TABLE, // a. table
                        COLUMNS, // b. column names
                        KEY_ID_DEN + " = ?", // c. selections
                        new String[]{String.valueOf(id)}, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        Den den = new Den(id, cursor.getString(cursor.getColumnIndex(KEY_NAZEV)),cursor.getString(cursor.getColumnIndex(KEY_ZKRATKA)));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return den;
    }

    public static Den getDenByNazev(String nazev) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        Cursor cursor =
                db.query(TABLE, // a. table
                        COLUMNS, // b. column names
                        KEY_NAZEV + " = ?", // c. selections
                        new String[]{String.valueOf(nazev)}, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        Den den = new Den(cursor.getInt(cursor.getColumnIndex(KEY_ID_DEN)), cursor.getString(cursor.getColumnIndex(KEY_NAZEV)),cursor.getString(cursor.getColumnIndex(KEY_ZKRATKA)));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return den;
    }

    public static Den getDenByZkratka(String zkratka) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        Cursor cursor =
                db.query(TABLE, // a. table
                        COLUMNS, // b. column names
                        KEY_ZKRATKA + " = ?", // c. selections
                        new String[]{String.valueOf(zkratka)}, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        Den den = new Den(cursor.getInt(cursor.getColumnIndex(KEY_ID_DEN)), cursor.getString(cursor.getColumnIndex(KEY_NAZEV)),cursor.getString(cursor.getColumnIndex(KEY_ZKRATKA)));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return den;
    }


}
