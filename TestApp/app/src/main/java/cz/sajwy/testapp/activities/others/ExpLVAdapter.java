package cz.sajwy.testapp.activities.others;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import cz.sajwy.testapp.R;
import cz.sajwy.testapp.data.model.CasovePravidlo;
import cz.sajwy.testapp.data.model.Den;
import cz.sajwy.testapp.data.model.KalendarovePravidlo;
import cz.sajwy.testapp.data.model.Kategorie;
import cz.sajwy.testapp.data.model.Pravidlo;
import cz.sajwy.testapp.data.repo.CasovePravidloRepo;
import cz.sajwy.testapp.data.repo.KalendarovePravidloRepo;
import cz.sajwy.testapp.data.repo.PolohovePravidloRepo;
import cz.sajwy.testapp.data.repo.PravidloRepo;
import cz.sajwy.testapp.data.repo.WifiPravidloRepo;

public class ExpLVAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Kategorie> kategorie;

    public ExpLVAdapter(Context context, ArrayList<Kategorie> kategorie) {
        this.context = context;
        this.kategorie = kategorie;
    }

    @Override
    public int getGroupCount() {
        return kategorie.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Pravidlo> pravidla = kategorie.get(groupPosition).getPravidla();
        return pravidla.size();
    }

    @Override
    public Kategorie getGroup(int groupPosition) {
        return kategorie.get(groupPosition);
    }

    @Override
    public Pravidlo getChild(int groupPosition, int childPosition) {
        ArrayList<Pravidlo> pravidla = kategorie.get(groupPosition).getPravidla();
        return pravidla.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Kategorie kategorie = getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.explv_seznam_pravidel_group, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.tvKategorie);
        tv.setText(kategorie.getNazev());
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
        final Pravidlo pravidlo = getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.explv_seznam_pravidel_child, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.tvPravidlo);
        tv.setText(pravidlo.getNazev().toString());

        Switch switchStav = (Switch) convertView.findViewById(R.id.switchStav);
        if(pravidlo.getStav() == 1)
            switchStav.setChecked(true);
        else
            switchStav.setChecked(false);

        switchStav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    pravidlo.setStav(1);
                    PravidloRepo.updateStavPravidla(pravidlo.getId_pravidlo(), pravidlo.getStav());
                } else {
                    pravidlo.setStav(0);
                    PravidloRepo.updateStavPravidla(pravidlo.getId_pravidlo(), pravidlo.getStav());
                }
                Kategorie k = kategorie.get(groupPosition);
                switch (k.getNazev()) {
                    case "Časové pravidlo":
                        CasovePravidlo casovePravidlo = CasovePravidloRepo.getCasovePravidlo(pravidlo.getId_pravidlo());
                        long[] casoveUdaje = Utils.prevedTimeStringyNaMilisekundy(casovePravidlo.getCas_od(), casovePravidlo.getDoba_trvani(), false);
                        long nyni = Calendar.getInstance().getTimeInMillis();
                        ArrayList<Den> dny = casovePravidlo.getDny();
                        Den den = Utils.zjistiAktualniDen();
                        if(Utils.jeDenObsazen(dny,den) && casoveUdaje[1] > nyni) {
                            boolean zapnoutZvuky = Utils.zapnoutZvuky(context, Calendar.getInstance(), 1);
                            Utils.zrusCasovace(context, zapnoutZvuky);
                            Utils.nastavCasovace(context, Calendar.getInstance());
                        }
                        break;
                    case "Kalendářové pravidlo":
                        KalendarovePravidlo kalendarovePravidlo = KalendarovePravidloRepo.getKalendarovePravidlo(pravidlo.getId_pravidlo());
                        if(Utils.jeUdalostDnes(context,kalendarovePravidlo.getUdalost())) {
                            boolean zapnoutZvuky = Utils.zapnoutZvuky(context, Calendar.getInstance(), 1);
                            Utils.zrusCasovace(context, zapnoutZvuky);
                            Utils.nastavCasovace(context, Calendar.getInstance());
                        }
                        break;
                    case "Polohové pravidlo":

                        break;
                    case "Wifi pravidlo":

                        break;
                }
            }
        });

        ImageView delete = (ImageView) convertView.findViewById(R.id.icSmaz);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogStyle);
                builder.setTitle(R.string.odstraneniPravidla);
                builder.setMessage(R.string.odstraneniPravidlaMessage);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ano,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Kategorie k = kategorie.get(groupPosition);
                                Pravidlo p = k.getPravidla().get(childPosition);

                                switch (k.getNazev()) {
                                    case "Časové pravidlo":
                                        CasovePravidlo casovePravidlo = CasovePravidloRepo.getCasovePravidlo(p.getId_pravidlo());
                                        long[] casoveUdaje = Utils.prevedTimeStringyNaMilisekundy(casovePravidlo.getCas_od(), casovePravidlo.getDoba_trvani(), false);
                                        long nyni = Calendar.getInstance().getTimeInMillis();
                                        ArrayList<Den> dny = casovePravidlo.getDny();
                                        Den den = Utils.zjistiAktualniDen();
                                        if(Utils.jeDenObsazen(dny, den) && casoveUdaje[1] > nyni && casovePravidlo.getStav() == 1) {
                                            boolean zapnoutZvuky = Utils.zapnoutZvuky(context, Calendar.getInstance(), 2);
                                            CasovePravidloRepo.deleteCasovePravidlo(p.getId_pravidlo());
                                            Utils.zrusCasovace(context,zapnoutZvuky);
                                            Utils.nastavCasovace(context, Calendar.getInstance());
                                        } else
                                        CasovePravidloRepo.deleteCasovePravidlo(p.getId_pravidlo());
                                        break;
                                    case "Kalendářové pravidlo":
                                        KalendarovePravidlo kp = KalendarovePravidloRepo.getKalendarovePravidlo(p.getId_pravidlo());
                                        if(Utils.jeUdalostDnes(context,kp.getUdalost()) && kp.getStav() == 1) {
                                            boolean zapnoutZvuky = Utils.zapnoutZvuky(context, Calendar.getInstance(), 2);
                                            KalendarovePravidloRepo.deleteKalendarovePravidlo(p.getId_pravidlo());
                                            Utils.zrusCasovace(context,zapnoutZvuky);
                                            Utils.nastavCasovace(context, Calendar.getInstance());
                                        } else
                                        KalendarovePravidloRepo.deleteKalendarovePravidlo(p.getId_pravidlo());
                                        break;
                                    case "Polohové pravidlo":
                                        PolohovePravidloRepo.deletePolohovePravidlo(p.getId_pravidlo());
                                        break;
                                    case "Wifi pravidlo":
                                        WifiPravidloRepo.deleteWifiPravidlo(p.getId_pravidlo());
                                        break;
                                }

                                k.getPravidla().remove(childPosition);
                                notifyDataSetChanged();

                                Toast.makeText(context, R.string.pravidloOdstraneno, Toast.LENGTH_SHORT).show();
                            }
                        });
                builder.setNegativeButton(R.string.ne,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
