package cz.sajwy.testapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.sajwy.testapp.R;
import cz.sajwy.testapp.activities.others.ExpLVAdapter;
import cz.sajwy.testapp.data.model.Kategorie;
import cz.sajwy.testapp.data.model.Pravidlo;
import cz.sajwy.testapp.data.repo.KategorieRepo;
import cz.sajwy.testapp.data.repo.PravidloRepo;

public class SeznamPravidelActivity extends AppCompatActivity {

    private ExpLVAdapter expLVAdapter;
    private ArrayList<Kategorie> expListItems;
    private ExpandableListView expandableListView;

    int groupPos;
    int childPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_seznam_pravidel);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        expListItems = KategorieRepo.getKategorieSPravidly();
        expLVAdapter = new ExpLVAdapter(SeznamPravidelActivity.this, expListItems);
        expandableListView.setAdapter(expLVAdapter);

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                String kategorie = expLVAdapter.getGroup(groupPosition).getNazev();
                Pravidlo pravidlo = expLVAdapter.getChild(groupPosition, childPosition);

                groupPos = groupPosition;
                childPos = childPosition;

                Intent intent = new Intent(getApplicationContext(), DetailPravidlaActivity.class);
                intent.putExtra("kategorie", kategorie);
                intent.putExtra("id", pravidlo.getId_pravidlo());
                startActivity(intent);

                return false;
            }
        });

        /*expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                String group_name = expListItems.get(groupPosition).getNazev();
                showToastMsg(group_name + " Expanded");
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                String group_name = expListItems.get(groupPosition).getNazev();
                showToastMsg(group_name + " Collapsed");
            }
        });*/
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Pravidlo p = expLVAdapter.getChild(groupPos, childPos);
        Pravidlo pn;
        try {
            pn = PravidloRepo.getPravidlo(p.getId_pravidlo());
        } catch (Exception e) {
            pn = null;
        }

        if(pn != null) {
            if (!p.getNazev().equals(pn.getNazev()) || p.getStav() != pn.getStav()) {
                expListItems.get(groupPos).getPravidla().set(childPos, pn);
                expLVAdapter.notifyDataSetChanged();
            }
        } else {
            expListItems.get(groupPos).getPravidla().remove(childPos);
            expLVAdapter.notifyDataSetChanged();

            Toast.makeText(this, R.string.pravidloOdstraneno, Toast.LENGTH_SHORT).show();
        }
    }
}
