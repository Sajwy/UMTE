package cz.sajwy.testapp.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import cz.sajwy.testapp.data.DBManager;
import cz.sajwy.testapp.data.model.Pravidlo;

public class PravidloRepo {
    private static final String TABLE = "Pravidlo";

    private static final String KEY_ID_PRAVIDLO = "ID_pravidlo";
    private static final String KEY_NAZEV = "Nazev";
    private static final String KEY_STAV = "Stav";
    private static final String KEY_VIBRACE = "Vibrace";
    private static final String KEY_FK_KATEGORIE = "FK_kategorie";

    private static final String[] COLUMNS = {KEY_ID_PRAVIDLO, KEY_NAZEV, KEY_STAV, KEY_VIBRACE, KEY_FK_KATEGORIE};

    public PravidloRepo() {
    }

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_ID_PRAVIDLO  + " INTEGER PRIMARY KEY," +
                    KEY_NAZEV + " TEXT, " +
                    KEY_STAV + " INTEGER, " +
                    KEY_VIBRACE + " INTEGER, " +
                    KEY_FK_KATEGORIE + " INTEGER REFERENCES Kategorie" +
                ");";
    }

    public static String createIndex() {
        return "CREATE INDEX ix_id_pravidlo ON " + TABLE + "(" + KEY_ID_PRAVIDLO + ");"+
                "CREATE INDEX ix_nazev_pravidla ON " + TABLE + "(" + KEY_NAZEV + ");";
    }

    public static int insertPravidlo(Pravidlo pravidlo) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "INSERT INTO " + TABLE + "(" + KEY_NAZEV + ", " + KEY_STAV + ", " + KEY_VIBRACE + ", " + KEY_FK_KATEGORIE + ") " +
                        "VALUES('" + pravidlo.getNazev() + "'," + pravidlo.getStav() + "," +
                                    pravidlo.getVibrace() + "," + pravidlo.getKategorie().getId_kategorie() + ");";

        db.execSQL(query);

        String selectID = "select last_insert_rowid();";
        Cursor cursor = db.rawQuery(selectID, null);

        int id = -1;
        if (cursor.moveToFirst())
            id = cursor.getInt(0);

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return id;
    }

    public static void updatePravidlo(Pravidlo novePravidlo) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "UPDATE " + TABLE + " SET " + KEY_NAZEV + " = '" + novePravidlo.getNazev() + "', " +
                                                    KEY_STAV + " = " + novePravidlo.getStav() + ", " +
                                                    KEY_VIBRACE + " = " + novePravidlo.getVibrace() + ", " +
                                                    KEY_FK_KATEGORIE + " = " + novePravidlo.getKategorie().getId_kategorie() +
                        " WHERE " + KEY_ID_PRAVIDLO + " = " + novePravidlo.getId_pravidlo();
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static void updateStavPravidla(int id, int novyStav) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "UPDATE " + TABLE + " SET " + KEY_STAV + " = " + novyStav +
                " WHERE " + KEY_ID_PRAVIDLO + " = " + id;
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static void deletePravidlo(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "DELETE FROM " + TABLE + " WHERE " + KEY_ID_PRAVIDLO + " = " + id;
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static Pravidlo getPravidlo(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        Cursor cursor = db.query(TABLE, // a. table
                COLUMNS, // b. column names
                KEY_ID_PRAVIDLO + " = ?", // c. selections
                new String[]{String.valueOf(id)}, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        Pravidlo pravidlo = new Pravidlo(id, cursor.getString(cursor.getColumnIndex(KEY_NAZEV)),
                                        cursor.getInt(cursor.getColumnIndex(KEY_STAV)),
                                        cursor.getInt(cursor.getColumnIndex(KEY_VIBRACE)),
                KategorieRepo.getKategorieByID(cursor.getInt(cursor.getColumnIndex(KEY_FK_KATEGORIE))));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return pravidlo;
    }

    public static ArrayList<Pravidlo> getPravidlaByKategorie(int idKategorie) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT * FROM " + TABLE + " WHERE " + KEY_FK_KATEGORIE + " = " + idKategorie + " ORDER BY " + KEY_STAV + " DESC, " + KEY_NAZEV;

        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Pravidlo> pravidla = new ArrayList<Pravidlo>();

        if (cursor.moveToFirst()) {
            do {
                Pravidlo pravidlo = new Pravidlo(cursor.getInt(cursor.getColumnIndex(KEY_ID_PRAVIDLO)),
                        cursor.getString(cursor.getColumnIndex(KEY_NAZEV)),
                        cursor.getInt(cursor.getColumnIndex(KEY_STAV)),
                        cursor.getInt(cursor.getColumnIndex(KEY_VIBRACE)),
                        KategorieRepo.getKategorieByID(cursor.getInt(cursor.getColumnIndex(KEY_FK_KATEGORIE))));

                pravidla.add(pravidlo);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return pravidla;
    }

    //prozatím pouze časová a kalendářová
    public static boolean existujiPravidla() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        String query = "SELECT COUNT(*) AS Pocet FROM " + TABLE + " WHERE " + KEY_FK_KATEGORIE + "=1 OR " + KEY_FK_KATEGORIE + "=2";
        Cursor cursor = db.rawQuery(query, null);
        int pocet;
        if (cursor.moveToFirst()) {
            pocet = cursor.getInt(cursor.getColumnIndex("Pocet"));
        } else
            pocet = 0;
        cursor.close();
        DBManager.getInstance().closeDatabase();

        boolean existuji;
        if(pocet == 0)
            existuji = false;
        else
            existuji = true;

        return existuji;
    }

    //není potřeba
    public static ArrayList<Pravidlo> getPravidla() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT * FROM " + TABLE + " ORDER BY " + KEY_FK_KATEGORIE + ", " + KEY_STAV + " DESC, " + KEY_NAZEV;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Pravidlo> pravidla = new ArrayList<Pravidlo>();

        if (cursor.moveToFirst()) {
            do {
                Pravidlo pravidlo = new Pravidlo(cursor.getInt(cursor.getColumnIndex(KEY_ID_PRAVIDLO)),
                                                cursor.getString(cursor.getColumnIndex(KEY_NAZEV)),
                                                cursor.getInt(cursor.getColumnIndex(KEY_STAV)),
                                                cursor.getInt(cursor.getColumnIndex(KEY_VIBRACE)),
                        KategorieRepo.getKategorieByID(cursor.getInt(cursor.getColumnIndex(KEY_FK_KATEGORIE))));

                pravidla.add(pravidlo);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return pravidla;
    }
}
