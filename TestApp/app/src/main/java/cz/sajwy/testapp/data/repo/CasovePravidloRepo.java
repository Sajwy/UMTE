package cz.sajwy.testapp.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;

import cz.sajwy.testapp.activities.others.Utils;
import cz.sajwy.testapp.data.DBManager;
import cz.sajwy.testapp.data.model.CasovePravidlo;
import cz.sajwy.testapp.data.model.Den;
import cz.sajwy.testapp.data.model.Pravidlo;

/**
 * Created by Sajwy on 27.05.2017.
 */
public class CasovePravidloRepo {
    private static final String TABLE = "CasovePravidlo";

    private static final String KEY_FK_PRAVIDLO = "ID_pravidlo";
    private static final String KEY_CAS_OD = "CasOd";
    private static final String KEY_DOBA_TRVANI = "DobaTrvani";

    private static final String[] COLUMNS = {KEY_FK_PRAVIDLO, KEY_CAS_OD, KEY_DOBA_TRVANI};

    public CasovePravidloRepo() {
    }

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_FK_PRAVIDLO  + " INTEGER REFERENCES Pravidlo," +
                    KEY_CAS_OD + " TEXT, " +
                    KEY_DOBA_TRVANI + " TEXT " +
                ");";
    }

    public static String createIndex() {
        return "CREATE INDEX ix_id_cp ON " + TABLE + "(" + KEY_FK_PRAVIDLO + ");";
    }

    public static void insertCasovePravidlo(CasovePravidlo casovePravidlo){
        int id = PravidloRepo.insertPravidlo(new Pravidlo(casovePravidlo.getNazev(), casovePravidlo.getStav(),
                casovePravidlo.getVibrace(), casovePravidlo.getKategorie()));

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "INSERT INTO " + TABLE + " (" + KEY_FK_PRAVIDLO + ", " + KEY_CAS_OD + ", " + KEY_DOBA_TRVANI + ") " +
                        " VALUES " + "(" + id + " ,'" + casovePravidlo.getCas_od() + "', '" + casovePravidlo.getDoba_trvani() + "');";

        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        DnyCasovehoPravidlaRepo.insert(id, casovePravidlo.getDny());
    }

    public static void updateCasovePravidlo(CasovePravidlo novePravidlo) {
        DnyCasovehoPravidlaRepo.update(novePravidlo.getId_pravidlo(), novePravidlo.getDny());

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "UPDATE " + TABLE + " SET " + KEY_CAS_OD + " = '" + novePravidlo.getCas_od() + "', " +
                                                    KEY_DOBA_TRVANI + " = '" + novePravidlo.getDoba_trvani() +
                        "' WHERE " + KEY_FK_PRAVIDLO + " = " + novePravidlo.getId_pravidlo();
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        Pravidlo pravidlo = new Pravidlo(novePravidlo.getId_pravidlo(), novePravidlo.getNazev(), novePravidlo.getStav(),
                novePravidlo.getVibrace(),novePravidlo.getKategorie());

        PravidloRepo.updatePravidlo(pravidlo);
    }

    public static void deleteCasovePravidlo(int id) {
        DnyCasovehoPravidlaRepo.delete(id);

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "DELETE FROM " + TABLE + " WHERE " + KEY_FK_PRAVIDLO + " = " + id;
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        PravidloRepo.deletePravidlo(id);
    }

    public static CasovePravidlo getCasovePravidlo(int id) {
        Pravidlo pravidlo = PravidloRepo.getPravidlo(id);

        ArrayList<Den> dny = getDnyCasovehoPravidla(DnyCasovehoPravidlaRepo.getIdDnu(id));

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        Cursor cursor = db.query(TABLE, // a. table
                COLUMNS, // b. column names
                KEY_FK_PRAVIDLO + " = ?", // c. selections
                new String[]{String.valueOf(id)}, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        CasovePravidlo casovePravidlo = new CasovePravidlo(id,pravidlo.getNazev(),pravidlo.getStav(),
                pravidlo.getVibrace(),pravidlo.getKategorie(),dny,cursor.getString(cursor.getColumnIndex(KEY_CAS_OD)),
                cursor.getString(cursor.getColumnIndex(KEY_DOBA_TRVANI)));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return casovePravidlo;
    }

    public static ArrayList<Den> getDnyCasovehoPravidla(ArrayList<Integer> idDnu) {
        ArrayList<Den> dnyPravidla = new ArrayList<>();
        Den den;
        for(int i = 0;i < idDnu.size();i++) {
            den = DenRepo.getDenByID(idDnu.get(i));
            dnyPravidla.add(den);
        }

        return dnyPravidla;
    }

    public static ArrayList<Integer> getIntListDnuCasovehoPravidla(int idPravidla) {
        ArrayList<Integer> intList = DnyCasovehoPravidlaRepo.getIdDnu(idPravidla);

        for(int i = 0;i < intList.size();i++)
            intList.set(i, intList.get(i) - 1);

        return intList;
    }

    public static ArrayList<CasovePravidlo> getCasovaPravidlaByDenAktualni(Den den) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT * FROM " + TABLE + " cp JOIN Pravidlo p ON cp." + KEY_FK_PRAVIDLO + " = p." + KEY_FK_PRAVIDLO +
                " JOIN DnyCasovehoPravidla dcp ON p." + KEY_FK_PRAVIDLO + " = dcp." + KEY_FK_PRAVIDLO +
                " WHERE dcp.ID_den = " + den.getId_den() + " AND p.Stav = 1" +
                " ORDER BY cp." + KEY_CAS_OD + ", " + KEY_DOBA_TRVANI + " DESC";

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<CasovePravidlo> pravidla = new ArrayList<>();
        CasovePravidlo casovePravidlo;
        long[] casoveUdaje;
        while (cursor.moveToNext()) {
            casoveUdaje = Utils.prevedTimeStringyNaMilisekundy(cursor.getString(cursor.getColumnIndex(KEY_CAS_OD)), cursor.getString(cursor.getColumnIndex(KEY_DOBA_TRVANI)), false);
            casovePravidlo = new CasovePravidlo(cursor.getInt(cursor.getColumnIndex("ID_pravidlo")),cursor.getString(cursor.getColumnIndex("Nazev")),
                    cursor.getInt(cursor.getColumnIndex("Stav")),cursor.getInt(cursor.getColumnIndex("Vibrace")),
                    KategorieRepo.getKategorieByID(cursor.getInt(cursor.getColumnIndex("FK_kategorie"))),
                    getDnyCasovehoPravidla(DnyCasovehoPravidlaRepo.getIdDnu(cursor.getInt(cursor.getColumnIndex("ID_pravidlo")))),
                    casoveUdaje[0],casoveUdaje[1]);

            pravidla.add(casovePravidlo);
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();
        return pravidla;
    }

    public static ArrayList<CasovePravidlo> getCasovaPravidlaByDenPredchozi(Den aktualniDen) {
        int idPredchozi = aktualniDen.getId_den() - 1;
        if(idPredchozi == 0)
            idPredchozi = 7;

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        /*String query = "SELECT * FROM Pravidlo p JOIN " + TABLE + " cp ON p." + KEY_FK_PRAVIDLO + " = cp." + KEY_FK_PRAVIDLO +
                " JOIN DnyCasovehoPravidla dcp ON cp." + KEY_FK_PRAVIDLO + " = dcp." + KEY_FK_PRAVIDLO +
                " WHERE dcp.ID_den = " + idPredchozi + " AND p.Stav = 1" + " AND (" +
                "(cast(substr('" + KEY_CAS_OD + "',1,2) AS INTEGER) + cast(substr('" + KEY_DOBA_TRVANI + "',1,2) AS INTEGER)) >= 24 OR " +
                "((cast(substr('" + KEY_CAS_OD + "',1,2) AS INTEGER) + cast(substr('" + KEY_DOBA_TRVANI + "',1,2) AS INTEGER)) = 23 AND " +
                "(cast(substr('" + KEY_CAS_OD + "',4,5) AS INTEGER) + cast(substr('" + KEY_DOBA_TRVANI + "',4,5) AS INTEGER)) > 59))" +
                " ORDER BY cp." + KEY_CAS_OD + ", cp." + KEY_DOBA_TRVANI + " DESC";*/

        String query = "SELECT * FROM Pravidlo p JOIN " + TABLE + " cp ON p." + KEY_FK_PRAVIDLO + " = cp." + KEY_FK_PRAVIDLO +
                " JOIN DnyCasovehoPravidla dcp ON cp." + KEY_FK_PRAVIDLO + " = dcp." + KEY_FK_PRAVIDLO +
                " WHERE dcp.ID_den = " + idPredchozi + " AND p.Stav = 1" +
                " ORDER BY cp." + KEY_CAS_OD + ", cp." + KEY_DOBA_TRVANI + " DESC";

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<CasovePravidlo> pravidla = new ArrayList<>();
        CasovePravidlo casovePravidlo;
        long[] casoveUdaje;
        while (cursor.moveToNext()) {
            casoveUdaje = Utils.prevedTimeStringyNaMilisekundy(cursor.getString(cursor.getColumnIndex(KEY_CAS_OD)), cursor.getString(cursor.getColumnIndex(KEY_DOBA_TRVANI)), true);
            casovePravidlo = new CasovePravidlo(cursor.getInt(cursor.getColumnIndex("ID_pravidlo")),cursor.getString(cursor.getColumnIndex("Nazev")),
                    cursor.getInt(cursor.getColumnIndex("Stav")),cursor.getInt(cursor.getColumnIndex("Vibrace")),
                    KategorieRepo.getKategorieByID(cursor.getInt(cursor.getColumnIndex("FK_kategorie"))),
                    getDnyCasovehoPravidla(DnyCasovehoPravidlaRepo.getIdDnu(cursor.getInt(cursor.getColumnIndex("ID_pravidlo")))),
                    casoveUdaje[0],casoveUdaje[1]);

            pravidla.add(casovePravidlo);
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();
        return pravidla;
    }

    /*
    public static ArrayList<CasovePravidlo> getCasovaPravidlaByDenAktualni(Den den) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT * FROM " + TABLE + " cp JOIN Pravidlo p ON cp." + KEY_FK_PRAVIDLO + " = p." + KEY_FK_PRAVIDLO +
                " JOIN DnyCasovehoPravidla dcp ON p." + KEY_FK_PRAVIDLO + " = dcp." + KEY_FK_PRAVIDLO +
                " WHERE dcp.ID_den = " + den.getId_den() + " AND p.Stav = 1" +
                " ORDER BY cp." + KEY_CAS_OD + ", " + KEY_DOBA_TRVANI + " DESC";

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<CasovePravidlo> pravidla = new ArrayList<>();
        CasovePravidlo casovePravidlo;
        while (cursor.moveToNext()) {
            casovePravidlo = new CasovePravidlo(cursor.getInt(cursor.getColumnIndex("ID_pravidlo")),cursor.getString(cursor.getColumnIndex("Nazev")),
                    cursor.getInt(cursor.getColumnIndex("Stav")),cursor.getInt(cursor.getColumnIndex("Vibrace")),
                    KategorieRepo.getKategorieByID(cursor.getInt(cursor.getColumnIndex("FK_kategorie"))),
                    getDnyCasovehoPravidla(DnyCasovehoPravidlaRepo.getIdDnu(cursor.getInt(cursor.getColumnIndex("ID_pravidlo")))),
                    cursor.getString(cursor.getColumnIndex(KEY_CAS_OD)),cursor.getString(cursor.getColumnIndex(KEY_DOBA_TRVANI)));

            pravidla.add(casovePravidlo);
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();
        return pravidla;
    }
     */
}
