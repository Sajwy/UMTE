package cz.sajwy.testapp.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import cz.sajwy.testapp.data.DBManager;

/**
 * Created by Sajwy on 07.06.2017.
 */
public class CasovaceRepo {
    private static final String TABLE = "Casovace";

    private static final String KEY_NASTAVENO = "Nastaveno";

    private static final String[] COLUMNS = {KEY_NASTAVENO};

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                KEY_NASTAVENO  + " INTEGER" +
                ");";
    }

    public static String insertData(){
        return  "INSERT INTO " + TABLE + " (" + KEY_NASTAVENO + ") VALUES (0);";
    }

    public static void update(int vykonavaSePravidlo) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "UPDATE " + TABLE + " SET " + KEY_NASTAVENO + " = " + vykonavaSePravidlo;

        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static int get() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT " + KEY_NASTAVENO + " FROM " + TABLE;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null)
            cursor.moveToFirst();

        int vykonavaSe = cursor.getInt(cursor.getColumnIndex(KEY_NASTAVENO));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return vykonavaSe;
    }
}
