package cz.sajwy.testapp.data.model;

import java.util.ArrayList;

public class Kategorie {
    private int id_kategorie;
    private String nazev;

    private ArrayList<Pravidlo> pravidla;

    public Kategorie() {
    }

    public Kategorie(int id_kategorie, String nazev) {
        this.id_kategorie = id_kategorie;
        this.nazev = nazev;
    }

    public Kategorie(int id_kategorie, String nazev, ArrayList<Pravidlo> pravidla) {
        this.id_kategorie = id_kategorie;
        this.nazev = nazev;
        this.pravidla = pravidla;
    }

    public Kategorie(String nazev) {
        this.nazev = nazev;
    }

    public int getId_kategorie() {
        return id_kategorie;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public ArrayList<Pravidlo> getPravidla() {
        return pravidla;
    }

    public void setPravidla(ArrayList<Pravidlo> pravidla) {
        this.pravidla = pravidla;
    }
}
