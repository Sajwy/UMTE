package cz.sajwy.testapp.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import cz.sajwy.testapp.data.DBManager;

/**
 * Created by Sajwy on 06.06.2017.
 */
public class IntentRepo {
    private static final String TABLE = "Intent";

    private static final String KEY_ID_INTENT = "ID_intent";

    private static final String[] COLUMNS = {KEY_ID_INTENT};

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_ID_INTENT  + " INTEGER" +
                ");";
    }

    public static void insert(int id){
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "INSERT INTO " + TABLE + " (" + KEY_ID_INTENT + ") VALUES (" + id + ");";
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static void delete(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        String query = "DELETE FROM " + TABLE + " WHERE " + KEY_ID_INTENT + " = " + id;
        db.execSQL(query);
        DBManager.getInstance().closeDatabase();
    }

    public static void deleteAll() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        String query = "DELETE FROM " + TABLE;
        db.execSQL(query);
        DBManager.getInstance().closeDatabase();
    }

    public static ArrayList<Integer> getAll() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT * FROM " + TABLE + " ORDER BY " + KEY_ID_INTENT;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Integer> ids = new ArrayList<>();

        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(KEY_ID_INTENT));
            ids.add(id);
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return ids;
    }
}
