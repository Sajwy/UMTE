package cz.sajwy.testapp.activities.others;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.CalendarContract;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import cz.sajwy.testapp.data.model.CasovePravidlo;
import cz.sajwy.testapp.data.model.Den;
import cz.sajwy.testapp.data.model.Kategorie;
import cz.sajwy.testapp.data.model.Udalost;
import cz.sajwy.testapp.data.repo.CasovaceRepo;
import cz.sajwy.testapp.data.repo.CasovePravidloRepo;
import cz.sajwy.testapp.data.repo.ConfigRepo;
import cz.sajwy.testapp.data.repo.DenRepo;
import cz.sajwy.testapp.data.repo.IntentRepo;
import cz.sajwy.testapp.data.repo.KalendarovePravidloRepo;
import cz.sajwy.testapp.data.repo.KategorieRepo;
import cz.sajwy.testapp.receiver.AlarmReceiver;

public class Utils {
    //vytáhnutí událostí z kalendáře
    private static final String[] CALENDAR_PROJECTION = new String[] { CalendarContract.Calendars.ACCOUNT_NAME };
    private static final String[] INSTANCE_PROJECTION = new String[] { CalendarContract.Instances.TITLE, CalendarContract.Instances.BEGIN,
                                                                        CalendarContract.Instances.END, CalendarContract.Instances.ALL_DAY};

    private static final int PROJECTION_ACCOUNT_NAME_INDEX = 0;
    private static final int PROJECTION_TITLE_INDEX = 0;
    private static final int PROJECTION_BEGIN_INDEX = 1;
    private static final int PROJECTION_END_INDEX = 2;
    private static final int PROJECTION_ALL_DAY_INDEX = 3;

    public static ArrayList<Udalost> getAllDayEvents(Context context, Calendar begin)
    {
        ArrayList<Udalost> vysledneUdalosti = new ArrayList<>();
        ArrayList<Udalost> kalendaroveUdalosti;

        Cursor calendarCursor = context.getContentResolver().query(Uri.parse("content://com.android.calendar/calendars"), CALENDAR_PROJECTION, null, null, null);
        while (calendarCursor.moveToNext())
        {
            String accountName = calendarCursor.getString(PROJECTION_ACCOUNT_NAME_INDEX);
            kalendaroveUdalosti = getCalendarDayEvents(context, accountName, begin);

            boolean vlozit = true;
            Udalost testovana;
            Udalost testujici;
            for(int i = 0;i < kalendaroveUdalosti.size();i++) {
                testovana = kalendaroveUdalosti.get(i);
                if(!vysledneUdalosti.contains(testovana)) {
                    testloop:
                    for (int j = 0; j < vysledneUdalosti.size(); j++) {
                        testujici = vysledneUdalosti.get(j);

                        if (testovana.getZacatek() >= testujici.getZacatek() && testovana.getKonec() <= testujici.getKonec()) {
                            vlozit = false;
                            break testloop;
                        }
                    }

                    if (vlozit) {
                        vysledneUdalosti.add(testovana);
                    } else {
                        vlozit = true;
                    }
                }
            }
        }
        calendarCursor.close();

        Collections.sort(vysledneUdalosti, new Comparator<Udalost>() {
            @Override
            public int compare(Udalost u1, Udalost u2) {
                return Long.compare(u1.getZacatek(), u2.getZacatek());
            }
        });

        return vysledneUdalosti;
    }

    //pomocná metoda ke getAllDayEvents..vytahuje události pro jednotlivé kalendáře
    private static ArrayList<Udalost> getCalendarDayEvents(Context context, String accountName, Calendar begin)
    {
        ArrayList<String> nazvyUdalosti = KalendarovePravidloRepo.getNazvyAktivnichUdalosti();
        ArrayList<Udalost> udalosti = new ArrayList<>();
        Calendar beginTime = Calendar.getInstance();
        Calendar endTime = Calendar.getInstance();

        beginTime.set(beginTime.get(Calendar.YEAR), beginTime.get(Calendar.MONTH), beginTime.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        endTime.set(beginTime.get(Calendar.YEAR), beginTime.get(Calendar.MONTH), beginTime.get(Calendar.DAY_OF_MONTH), 23, 59, 59);

        long startMillis = beginTime.getTimeInMillis();
        long endMillis = endTime.getTimeInMillis();

        Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
        ContentUris.appendId(builder, startMillis);
        ContentUris.appendId(builder, endMillis);

        String selection = "(((" + CalendarContract.Instances.END + " > ? AND " +
                                CalendarContract.Instances.START_DAY + " = ? AND " +
                                CalendarContract.Instances.BEGIN + " <= ? )" +
                                " OR (" + CalendarContract.Instances.START_DAY + " = ? AND " +
                                CalendarContract.Instances.END_DAY + " = ? AND " +
                                CalendarContract.Instances.END + " > ? AND " +
                                CalendarContract.Instances.END + " <= ? )) AND " +
                                CalendarContract.Calendars.ACCOUNT_NAME + " = ?)";

        Calendar predchoziDen = Calendar.getInstance();
        predchoziDen.set(predchoziDen.get(Calendar.YEAR), predchoziDen.get(Calendar.MONTH), predchoziDen.get(Calendar.DAY_OF_MONTH) - 1, 1, 1, 1);

        String[] selectionArgs = new String[] { begin.getTimeInMillis()+"", julianDay(Calendar.getInstance()), endMillis+"", julianDay(predchoziDen), julianDay(Calendar.getInstance()), begin.getTimeInMillis()+"", endMillis+"", accountName };
        Cursor cursor = context.getContentResolver().query(builder.build(), INSTANCE_PROJECTION, selection, selectionArgs, CalendarContract.Instances.BEGIN + " ASC, " + CalendarContract.Instances.END + " DESC ");

        while (cursor.moveToNext()) {
            Udalost u;
            String title;
            boolean vlozit = false;
            if(cursor.getInt(PROJECTION_ALL_DAY_INDEX) != 1 && cursor.getString(PROJECTION_END_INDEX) != null &&
                    cursor.getString(PROJECTION_BEGIN_INDEX) != null) {
                title = cursor.getString(PROJECTION_TITLE_INDEX);
                testloop:
                for(int i = 0;i < nazvyUdalosti.size();i++) {
                    if(title.toLowerCase().contains(nazvyUdalosti.get(i).toLowerCase())) {
                        vlozit = true;
                        break testloop;
                    }
                }
                if (vlozit) {
                    u = new Udalost(cursor.getString(PROJECTION_TITLE_INDEX),cursor.getLong(PROJECTION_BEGIN_INDEX),cursor.getLong(PROJECTION_END_INDEX));
                    udalosti.add(u);
                }
            }
        }
        cursor.close();

        return udalosti;
    }

    //pomocná metoda ke getCalendarDayEvents, která vrací juliánský den
    private static String julianDay(Calendar date) {
        int a = (14 - date.get(Calendar.MONTH) + 1) / 12;
        int y = date.get(Calendar.YEAR) + 4800 - a;
        int m = date.get(Calendar.MONTH) + 1 + 12 * a - 3;
        int jdn = date.get(Calendar.DAY_OF_MONTH) + (153 * m + 2)/5 + 365*y + y/4 - y/100 + y/400 - 32045;
        return jdn+"";
    }

    //převod událostí na časová pravidla
    public static ArrayList<CasovePravidlo> prevedUdalostiNaCasovaPravidla(ArrayList<Udalost> udalosti) {
        ArrayList<CasovePravidlo> casovaPravidla = new ArrayList<>();
        Den den = zjistiAktualniDen();
        ArrayList<Den> dny = new ArrayList<>();
        dny.add(den);
        CasovePravidlo pravidlo;
        Udalost u;
        for(int i = 0;i < udalosti.size();i++) {
            u = udalosti.get(i);
            Kategorie k = KategorieRepo.getKategorieByNazev("Kalendářové pravidlo");
            //String od = prevedMilisekundyNaTimeString(u.getZacatek());
            //String doba = prevedRozdilMilisekundNaTimeString(u.getKonec() - u.getZacatek());
            int vibrace = zjistiVibrace(u.getNazev());
            //pravidlo = new CasovePravidlo(0, u.getNazev(), 1, vibrace, k, dny, od, doba);
            pravidlo = new CasovePravidlo(0,u.getNazev(),1,vibrace,k,dny,u.getZacatek(),u.getKonec());
            casovaPravidla.add(pravidlo);
        }
        return casovaPravidla;
    }

    //test při CRUD operacích kal. pravidla, zda bude toto pravidlo použito ještě dnes
    public static boolean jeUdalostDnes(Context context, String slovoUdalosti) {
        ArrayList<Udalost> udalosti = getAllDayEvents(context, Calendar.getInstance());
        boolean vysledek = false;
        if(udalosti.size() > 0) {
            Udalost u;
            testloop:
            for(int i = 0;i < udalosti.size();i++) {
                u = udalosti.get(i);
                if(u.getNazev().toLowerCase().contains(slovoUdalosti.toLowerCase())) {
                    vysledek = true;
                    break testloop;
                }
            }
        } else {
            vysledek = false;
        }

        return vysledek;
    }

    private static String prevedMilisekundyNaTimeString(long mili) {
        Date date = new Date(mili);
        SimpleDateFormat formatter= new SimpleDateFormat("HH:mm");
        String timeString = formatter.format(date);
        return timeString;
    }

    private static String prevedRozdilMilisekundNaTimeString(long mili) {
        mili /= 1000;
        mili /= 60;
        int minuty = (int) mili % 60;
        mili /= 60;
        int hodiny = (int) mili % 24;
        return pad(hodiny)+":"+pad(minuty);
    }

    public static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private static int zjistiVibrace(String titulekUdalosti) {
        int vibrace = 0;
        ArrayList<String> nazvyUdalosti = KalendarovePravidloRepo.getNazvyAktivnichUdalosti();

        testloop:
        for(int i = 0;i < nazvyUdalosti.size();i++) {
            if(titulekUdalosti.toLowerCase().contains(nazvyUdalosti.get(i).toLowerCase())) {
                vibrace = KalendarovePravidloRepo.getVibrace(nazvyUdalosti.get(i));
                break testloop;
            }
        }
        return vibrace;
    }

//**************************************************************************************************************
    //vytáhnutí časových pravidel
    public static ArrayList<CasovePravidlo> vyberCasovaPravidla(Calendar begin) {
        ArrayList<CasovePravidlo> vyslednaCasovaPravidla = new ArrayList<>();
        Den den = zjistiAktualniDen();
        ArrayList<CasovePravidlo> predchoziCasovaPravidla = protridPredchoziCasovaPravidla(begin);
        ArrayList<CasovePravidlo> casovaPravidla = CasovePravidloRepo.getCasovaPravidlaByDenAktualni(den);
        ArrayList<CasovePravidlo> vsechnaPravidla = spojKolekcePravidel(predchoziCasovaPravidla,casovaPravidla);

        boolean vlozit = true;
        CasovePravidlo testovane;
        CasovePravidlo testujici;
        for(int i = 0;i < vsechnaPravidla.size();i++) {
            testovane = vsechnaPravidla.get(i);
            if(testovane.getCas_do_long() > begin.getTimeInMillis()) {
                testloop:
                for (int j = 0; j < vyslednaCasovaPravidla.size(); j++) {
                    testujici = vyslednaCasovaPravidla.get(j);
                    if (testovane.getCas_od_long() >= testujici.getCas_od_long() && testovane.getCas_do_long() <= testujici.getCas_do_long()) {
                        vlozit = false;
                        break testloop;
                    }
                }

                if (vlozit) {
                    vyslednaCasovaPravidla.add(testovane);
                } else {
                    vlozit = true;
                }
            }
        }

        return vyslednaCasovaPravidla;
    }

    private static ArrayList<CasovePravidlo> protridPredchoziCasovaPravidla(Calendar begin) {
        ArrayList<CasovePravidlo> vyslednaPravidla = new ArrayList<>();
        ArrayList<CasovePravidlo> predchoziPravidla = new ArrayList<>();
        ArrayList<CasovePravidlo> vsechnaPredchoziPravidla = CasovePravidloRepo.getCasovaPravidlaByDenPredchozi(zjistiAktualniDen());

        Calendar pulnoc = Calendar.getInstance();
        pulnoc.set(pulnoc.get(Calendar.YEAR), pulnoc.get(Calendar.MONTH), pulnoc.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        CasovePravidlo p;
        for(int i = 0;i < vsechnaPredchoziPravidla.size();i++) {
            p = vsechnaPredchoziPravidla.get(i);
            if(p.getCas_od_long() < pulnoc.getTimeInMillis() && pulnoc.getTimeInMillis() <= p.getCas_do_long() && begin.getTimeInMillis() < p.getCas_do_long())
                predchoziPravidla.add(p);
        }

        boolean vlozit = true;
        CasovePravidlo testovane;
        CasovePravidlo testujici;
        for(int i = 0;i < predchoziPravidla.size();i++) {
            testovane = predchoziPravidla.get(i);
            testloop:
            for (int j = 0; j < vyslednaPravidla.size(); j++) {
                testujici = vyslednaPravidla.get(j);
                if (testovane.getCas_od_long() >= testujici.getCas_od_long() && testovane.getCas_do_long() <= testujici.getCas_do_long()) {
                    vlozit = false;
                    break testloop;
                }
            }

            if (vlozit) {
                vyslednaPravidla.add(testovane);
            } else {
                vlozit = true;
            }
        }

        return vyslednaPravidla;
    }

    /*public static long[] prevedTimeStringyNaMilisekundy(String cas_od, String doba_trvani) {
        long[] milisekundyPole = new long[2];

        int[] casOdPole = rozdelTimeString(cas_od);

        int hodZacatek = casOdPole[0];
        int minZacatek = casOdPole[1];

        Calendar zacatek = Calendar.getInstance();
        zacatek.set(zacatek.get(Calendar.YEAR), zacatek.get(Calendar.MONTH), zacatek.get(Calendar.DAY_OF_MONTH), hodZacatek,
                minZacatek, 0);

        int[] dobaTrvaniPole = rozdelTimeString(doba_trvani);

        int hodDobaTrvani = dobaTrvaniPole[0];
        int minDobaTrvani = dobaTrvaniPole[1];

        int[] hodMinKonecPole = dopocitejKonec(hodZacatek,hodDobaTrvani,minZacatek,minDobaTrvani);

        int hodKonec = hodMinKonecPole[0];
        int minKonec = hodMinKonecPole[1];

        int preteceniDne = 0;
        if(hodKonec < hodZacatek || (hodKonec == hodZacatek && minKonec < minZacatek))
            preteceniDne = 1;

        Calendar konec = Calendar.getInstance();
        konec.set(zacatek.get(Calendar.YEAR), zacatek.get(Calendar.MONTH),zacatek.get(Calendar.DAY_OF_MONTH) + preteceniDne,
                hodKonec, minKonec, 0);

        milisekundyPole[0] = zacatek.getTimeInMillis();
        milisekundyPole[1] = konec.getTimeInMillis();

        return milisekundyPole;
    }*/

    public static long[] prevedTimeStringyNaMilisekundy(String cas_od, String doba_trvani, boolean predchoziDen) {
        long[] milisekundyPole = new long[2];

        int[] casOdPole = rozdelTimeString(cas_od);

        int hodZacatek = casOdPole[0];
        int minZacatek = casOdPole[1];

        Calendar zacatek = Calendar.getInstance();
        if(predchoziDen)
            zacatek.set(zacatek.get(Calendar.YEAR), zacatek.get(Calendar.MONTH), zacatek.get(Calendar.DAY_OF_MONTH) - 1, hodZacatek, minZacatek, 0);
        else
            zacatek.set(zacatek.get(Calendar.YEAR), zacatek.get(Calendar.MONTH), zacatek.get(Calendar.DAY_OF_MONTH), hodZacatek, minZacatek, 0);

        int[] dobaTrvaniPole = rozdelTimeString(doba_trvani);

        int hodDobaTrvani = dobaTrvaniPole[0];
        int minDobaTrvani = dobaTrvaniPole[1];

        int[] hodMinKonecPole = dopocitejKonec(hodZacatek,hodDobaTrvani,minZacatek,minDobaTrvani);

        int hodKonec = hodMinKonecPole[0];
        int minKonec = hodMinKonecPole[1];

        int preteceniDne = 0;
        if(hodKonec < hodZacatek || (hodKonec == hodZacatek && minKonec < minZacatek))
            preteceniDne = 1;

        Calendar konec = Calendar.getInstance();
        konec.set(zacatek.get(Calendar.YEAR), zacatek.get(Calendar.MONTH),zacatek.get(Calendar.DAY_OF_MONTH) + preteceniDne,
                hodKonec, minKonec, 0);

        milisekundyPole[0] = zacatek.getTimeInMillis();
        milisekundyPole[1] = konec.getTimeInMillis();

        return milisekundyPole;
    }

    private static int[] dopocitejKonec(int hZacatek, int hDoba, int mZacatek, int mDoba) {
        int minuty_do = (mZacatek + mDoba) % 60;
        int hodiny_do = ((mZacatek + mDoba)/60 + hZacatek + hDoba) % 24;
        int[] hodMinPole = {hodiny_do, minuty_do};
        return hodMinPole;
    }

    private static int[] rozdelTimeString(String timeString) {
        String[] castiRetezce = timeString.split("\\:");
        int hodiny = Integer.parseInt(castiRetezce[0]);
        int minuty = Integer.parseInt(castiRetezce[1]);
        int rozdelenyCas[] = {hodiny, minuty};
        return rozdelenyCas;
    }

    public static Den zjistiAktualniDen() {
        SimpleDateFormat czDenFormat = new SimpleDateFormat("EEEE", new Locale("cs", "CZ"));
        String denLowerString = czDenFormat.format(new Date());
        String denString = denLowerString.substring(0,1).toUpperCase() + denLowerString.substring(1);
        return DenRepo.getDenByNazev(denString);
    }

    public static boolean jeDenObsazen(ArrayList<Den> dny, Den den) {
        boolean obsazen = false;
        Den d;
        for(int i = 0;i < dny.size();i++) {
            d = dny.get(i);
            if(d.getId_den() == den.getId_den()) {
                obsazen = true;
            }
        }
        return obsazen;
    }

    //******************************************************************************************************************
    //spojení časových pravidel s převedenými událostmi na časová pravidla
    private static ArrayList<CasovePravidlo> spojKolekcePravidel(ArrayList<CasovePravidlo> casovaPravidla, ArrayList<CasovePravidlo> udalosti) {
        ArrayList<CasovePravidlo> vyslednaKolekce = new ArrayList<>();

        vyslednaKolekce.addAll(casovaPravidla);
        vyslednaKolekce.addAll(udalosti);

        return vyslednaKolekce;
    }

    //******************************************************************************************************************
    //vrácení výsledné spojené kolekce podle které se bude obsluhovat servica
    public static ArrayList<CasovePravidlo> vratVyslednouKolekciCasovychPravidel(Context context, Calendar zacatek) {
        ArrayList<CasovePravidlo> vyslednaPravidla = new ArrayList<>();

        ArrayList<CasovePravidlo> pravidla = spojKolekcePravidel(vyberCasovaPravidla(zacatek),prevedUdalostiNaCasovaPravidla(getAllDayEvents(context, zacatek)));

        Collections.sort(pravidla, new Comparator<CasovePravidlo>() {
            @Override
            public int compare(CasovePravidlo cp1, CasovePravidlo cp2) {
                return new CompareToBuilder().append(cp1.getCas_od_long(), cp2.getCas_od_long()).append(cp2.getCas_do_long(), cp1.getCas_do_long()).toComparison();
            }
        });

        boolean vlozit = true;
        CasovePravidlo testovane;
        CasovePravidlo testujici;
        for(int i = 0;i < pravidla.size();i++) {
            testovane = pravidla.get(i);
            testloop:
            for(int j = 0;j < vyslednaPravidla.size();j++) {
                testujici = vyslednaPravidla.get(j);
                if(testovane.getCas_od_long() >= testujici.getCas_od_long() && testovane.getCas_do_long() <= testujici.getCas_do_long()) {
                    vlozit = false;
                    break testloop;
                }
            }

            if(vlozit) {
                vyslednaPravidla.add(testovane);
            } else {
                vlozit = true;
            }
        }

        return vyslednaPravidla;
    }

    //****************************************************************************************
    //nastavení časovačů
    public static void nastavCasovace(Context context, Calendar dobaKdy) {
        ArrayList<CasovePravidlo> pravidla = vratVyslednouKolekciCasovychPravidel(context, dobaKdy);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int id = 0;
        CasovePravidlo casovePravidlo;
        Intent intent;
        PendingIntent pendingIntent;

        Calendar pulnoc = urciCas(1,0,0,0);
        intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("id",id);
        pendingIntent = PendingIntent.getBroadcast(context, id, intent, PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, pulnoc.getTimeInMillis(), pendingIntent);
        IntentRepo.insert(id);

        id++;

        for(int i = 0;i < pravidla.size();i++) {
            casovePravidlo = pravidla.get(i);

            intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("id",id);
            if(casovePravidlo.getVibrace() == 1)
                intent.putExtra("rezim", "vibrace");
            else
                intent.putExtra("rezim","ticho");
            pendingIntent = PendingIntent.getBroadcast(context,id,intent,PendingIntent.FLAG_ONE_SHOT);
            if(casovePravidlo.getCas_od_long() < new Date().getTime())
                alarmManager.set(AlarmManager.RTC_WAKEUP,new Date().getTime(),pendingIntent);
            else
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, casovePravidlo.getCas_od_long(), pendingIntent);
            IntentRepo.insert(id);
            id++;

            intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("id",id);
            intent.putExtra("rezim","normal");
            pendingIntent = PendingIntent.getBroadcast(context,id,intent,PendingIntent.FLAG_ONE_SHOT);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, casovePravidlo.getCas_do_long(), pendingIntent);
            IntentRepo.insert(id);
            id++;
        }
        CasovaceRepo.update(1);
    }

    //cancel pendingIntentu
    public static void zrusCasovace(Context context, boolean zapnoutZvuky) {
        if(/*ConfigRepo.get() == 1 &&*/ zapnoutZvuky) {
            AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            //ConfigRepo.update(0);
        }
        ConfigRepo.update(0);
        ArrayList<Integer> ids = IntentRepo.getAll();

        if(!ids.isEmpty()) {
            int id;
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent;
            PendingIntent pendingIntent;
            for(int i = 0;i < ids.size();i++) {
                id = ids.get(i);
                intent = new Intent(context, AlarmReceiver.class);
                pendingIntent = PendingIntent.getBroadcast(context,id,intent,PendingIntent.FLAG_ONE_SHOT);
                pendingIntent.cancel();
                alarmManager.cancel(pendingIntent);
            }
            IntentRepo.deleteAll();
        }
        CasovaceRepo.update(0);
    }

    public static Calendar urciCas(int prirustekDnu, int hod, int min, int sek) {
        Calendar zacatek = Calendar.getInstance();
        zacatek.set(zacatek.get(Calendar.YEAR), zacatek.get(Calendar.MONTH), zacatek.get(Calendar.DAY_OF_MONTH) + prirustekDnu, hod, min, sek);
        return zacatek;
    }

    public static boolean zapnoutZvuky(Context context, Calendar kdy, int pocetProPodminku) {
        ArrayList<CasovePravidlo> pravidla = vratVyslednouKolekciCasovychPravidel(context, kdy);
        int pocet = 0;
        for(int i = 0;i < pravidla.size();i++) {
            CasovePravidlo cp = pravidla.get(i);
            if(cp.getCas_od_long() <= kdy.getTimeInMillis() && kdy.getTimeInMillis() < cp.getCas_do_long())
                pocet++;
        }
        boolean vysledek;
        if(pocet < pocetProPodminku)
            vysledek = true;
        else
            vysledek = false;
        return vysledek;
    }
}
