package cz.sajwy.testapp.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import cz.sajwy.testapp.data.DBManager;
import cz.sajwy.testapp.data.model.PolohovePravidlo;
import cz.sajwy.testapp.data.model.Pravidlo;

public class PolohovePravidloRepo {
    private static final String TABLE = "PolohovePravidlo";

    private static final String KEY_FK_PRAVIDLO = "ID_pravidlo";
    private static final String KEY_LAT = "Latitude";
    private static final String KEY_LON = "Longitude";
    private static final String KEY_RADIUS = "Radius";

    private static final String[] COLUMNS = {KEY_FK_PRAVIDLO, KEY_LAT, KEY_LON, KEY_RADIUS};

    public PolohovePravidloRepo() {
    }

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_FK_PRAVIDLO  + " INTEGER REFERENCES Pravidlo," +
                    KEY_LAT + " REAL, " +
                    KEY_LON + " REAL, " +
                    KEY_RADIUS + " INTEGER " +
                ");";
    }

    public static String createIndex() {
        return "CREATE INDEX ix_id_pp ON " + TABLE + "(" + KEY_FK_PRAVIDLO + ");";
    }

    public static PolohovePravidlo getPolohovePravidlo(int id) {
        Pravidlo pravidlo = PravidloRepo.getPravidlo(id);

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        Cursor cursor = db.query(TABLE, // a. table
                COLUMNS, // b. column names
                KEY_FK_PRAVIDLO + " = ?", // c. selections
                new String[]{String.valueOf(id)}, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        PolohovePravidlo polohovePravidlo = new PolohovePravidlo(id,pravidlo.getNazev(),pravidlo.getStav(),
                pravidlo.getVibrace(),pravidlo.getKategorie(),cursor.getDouble(cursor.getColumnIndex(KEY_LAT)),
                cursor.getDouble(cursor.getColumnIndex(KEY_LON)),cursor.getInt(cursor.getColumnIndex(KEY_RADIUS)));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return polohovePravidlo;
    }

    public static void insertPolohovePravidlo(PolohovePravidlo polohovePravidlo){
        int id = PravidloRepo.insertPravidlo(new Pravidlo(polohovePravidlo.getNazev(), polohovePravidlo.getStav(),
                polohovePravidlo.getVibrace(), polohovePravidlo.getKategorie()));

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "INSERT INTO " + TABLE + " (" + KEY_FK_PRAVIDLO + ", " + KEY_LAT + ", " + KEY_LON + ", " + KEY_RADIUS + ") " +
                        "VALUES " + "(" + id + " ," + polohovePravidlo.getLatitude() + " ," + polohovePravidlo.getLongitude() +
                                " ," + polohovePravidlo.getRadius() + ");";

        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static void updatePolohovePravidlo(PolohovePravidlo novePravidlo) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "UPDATE " + TABLE + " SET " + KEY_LAT + " = " + novePravidlo.getLatitude() + ", " +
                                                    KEY_LON + " = " + novePravidlo.getLongitude() + ", " +
                                                    KEY_RADIUS + " = " + novePravidlo.getRadius() +
                " WHERE " + KEY_FK_PRAVIDLO + " = " + novePravidlo.getId_pravidlo();
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        Pravidlo pravidlo = new Pravidlo(novePravidlo.getId_pravidlo(), novePravidlo.getNazev(), novePravidlo.getStav(),
                novePravidlo.getVibrace(),novePravidlo.getKategorie());
        PravidloRepo.updatePravidlo(pravidlo);
    }

    public static void deletePolohovePravidlo(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "DELETE FROM " + TABLE + " WHERE " + KEY_FK_PRAVIDLO + " = " + id;
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        PravidloRepo.deletePravidlo(id);
    }
}
