package cz.sajwy.testapp.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.sajwy.testapp.R;
import cz.sajwy.testapp.activities.others.LVAdapter;
import cz.sajwy.testapp.activities.others.Utils;
import cz.sajwy.testapp.data.model.CasovePravidlo;
import cz.sajwy.testapp.data.model.Den;
import cz.sajwy.testapp.data.model.KalendarovePravidlo;
import cz.sajwy.testapp.data.model.PolohovePravidlo;
import cz.sajwy.testapp.data.model.Pravidlo;
import cz.sajwy.testapp.data.model.WifiPravidlo;
import cz.sajwy.testapp.data.repo.CasovePravidloRepo;
import cz.sajwy.testapp.data.repo.ConfigRepo;
import cz.sajwy.testapp.data.repo.DenRepo;
import cz.sajwy.testapp.data.repo.KalendarovePravidloRepo;
import cz.sajwy.testapp.data.repo.PolohovePravidloRepo;
import cz.sajwy.testapp.data.repo.PravidloRepo;
import cz.sajwy.testapp.data.repo.WifiPravidloRepo;
import cz.sajwy.testapp.service.ObsluhaPravidelService;

public class DetailPravidlaActivity extends AppCompatActivity {
    String kategorie;
    int id;

    ListView lvDetailPravidla;
    Pravidlo pravidlo;
    CasovePravidlo casovePravidlo;
    KalendarovePravidlo kalendarovePravidlo;
    WifiPravidlo wifiPravidlo;
    PolohovePravidlo polohovePravidlo;
    ArrayList<String> parametryVse;
    LVAdapter adapter;
    TextView tvNadpis, tvObsah, tvSwitchNadpis, tvSwitchObsah, tvPoloha, tvLat, tvLon;
    Switch prepinac;
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    WifiManager wifiManager;
    boolean boolVlastnorucne;
    boolean wifiScanPovolen;

    ArrayList<Integer> vybraneDnyBackup;
    ArrayList<Den> dny;

    String nazev;
    int stav;
    int vibrace;
    ArrayList<Integer> vybraneDny;
    String cas_od;
    String doba_trvani;
    String nazev_wifi;
    double latitude;
    double longitude;
    int radius;
    String udalost;
    boolean lze = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pravidla);

        Intent intent = this.getIntent();
        kategorie = intent.getStringExtra("kategorie");
        id = intent.getIntExtra("id", 0);

        pravidlo = PravidloRepo.getPravidlo(id);
        nazev = pravidlo.getNazev();
        stav = pravidlo.getStav();
        vibrace = pravidlo.getVibrace();

        switch (kategorie) {
            case "Časové pravidlo":
                casovePravidlo = CasovePravidloRepo.getCasovePravidlo(id);
                parametryVse = zjistiParametryPravidla(casovePravidlo, kategorie);
                cas_od = casovePravidlo.getCas_od();
                doba_trvani = casovePravidlo.getDoba_trvani();
                dny = DenRepo.getDny();
                vybraneDny = CasovePravidloRepo.getIntListDnuCasovehoPravidla(casovePravidlo.getId_pravidlo());
                vybraneDnyBackup = (ArrayList<Integer>) vybraneDny.clone();
                break;
            case "Kalendářové pravidlo":
                kalendarovePravidlo = KalendarovePravidloRepo.getKalendarovePravidlo(id);
                parametryVse = zjistiParametryPravidla(kalendarovePravidlo, kategorie);
                udalost = kalendarovePravidlo.getUdalost();
                break;
            case "Polohové pravidlo":
                polohovePravidlo = PolohovePravidloRepo.getPolohovePravidlo(id);
                parametryVse = zjistiParametryPravidla(polohovePravidlo, kategorie);
                latitude = polohovePravidlo.getLatitude();
                longitude = polohovePravidlo.getLongitude();
                radius = polohovePravidlo.getRadius();
                break;
            case "Wifi pravidlo":
                wifiPravidlo = WifiPravidloRepo.getWifiPravidlo(id);
                parametryVse = zjistiParametryPravidla(wifiPravidlo, kategorie);
                nazev_wifi = wifiPravidlo.getNazev_wifi();
                break;
            default:
                break;
        }

        lvDetailPravidla = (ListView) findViewById(R.id.lvDetailPravidla);
        adapter = new LVAdapter(getApplicationContext(), R.layout.lv_nove_pravidlo_tv, parametryVse, id);
        lvDetailPravidla.setAdapter(adapter);
        lvDetailPravidla.setOnItemClickListener(itemClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_pravidla, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                ulozPravidlo();
                break;
            case R.id.action_delete:
                smazPravidlo();
                break;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }

        return true;
    }

    private void smazPravidlo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailPravidlaActivity.this, R.style.DialogStyle);
        builder.setTitle(R.string.odstraneniPravidla);
        builder.setMessage(R.string.odstraneniPravidlaMessage);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ano,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idecko) {
                        switch (kategorie) {
                            case "Časové pravidlo":
                                CasovePravidlo casovePravidlo = CasovePravidloRepo.getCasovePravidlo(id);
                                long[] casoveUdaje = Utils.prevedTimeStringyNaMilisekundy(casovePravidlo.getCas_od(), casovePravidlo.getDoba_trvani(), false);
                                long nyni = Calendar.getInstance().getTimeInMillis();
                                ArrayList<Den> dny = casovePravidlo.getDny();
                                Den den = Utils.zjistiAktualniDen();
                                if(Utils.jeDenObsazen(dny, den) && casoveUdaje[1] > nyni && casovePravidlo.getStav() == 1) {
                                    boolean zapnoutZvuky = Utils.zapnoutZvuky(getApplicationContext(), Calendar.getInstance(), 2);
                                    CasovePravidloRepo.deleteCasovePravidlo(id);
                                    Utils.zrusCasovace(getApplicationContext(),zapnoutZvuky);
                                    Utils.nastavCasovace(getApplicationContext(), Calendar.getInstance());
                                } else
                                    CasovePravidloRepo.deleteCasovePravidlo(id);
                                break;
                            case "Kalendářové pravidlo":
                                KalendarovePravidlo kp = KalendarovePravidloRepo.getKalendarovePravidlo(id);
                                if(Utils.jeUdalostDnes(getApplicationContext(),kp.getUdalost())&& kp.getStav() == 1) {
                                    boolean zapnoutZvuky = Utils.zapnoutZvuky(getApplicationContext(), Calendar.getInstance(), 2);
                                    KalendarovePravidloRepo.deleteKalendarovePravidlo(id);
                                    Utils.zrusCasovace(getApplicationContext(), zapnoutZvuky);
                                    Utils.nastavCasovace(getApplicationContext(), Calendar.getInstance());
                                } else
                                    KalendarovePravidloRepo.deleteKalendarovePravidlo(id);
                                break;
                            case "Polohové pravidlo":
                                PolohovePravidloRepo.deletePolohovePravidlo(id);
                                break;
                            case "Wifi pravidlo":
                                WifiPravidloRepo.deleteWifiPravidlo(id);
                                break;
                        }

                        NavUtils.navigateUpFromSameTask(DetailPravidlaActivity.this);
                    }
                });
        builder.setNegativeButton(R.string.ne,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idecko) {
                        dialog.cancel();
                    }
                });

        alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
    }

    private void ulozPravidlo() {
        String validace = zvalidujVstupy();

        if(!validace.equals("") || lze == false) {
            if(lze == false && !validace.equals("")) {
                validace = "Nevyplněn parametr: " + validace.substring(0, validace.length() - 2) + "!!!\n\n";
                validace += "Název události je již použit!!!";
            } else if(lze == false)
                validace += "Název události je již použit!!!";
            else
                validace = "Nevyplněn parametr: " + validace.substring(0, validace.length() - 2) + "!!!";
            Toast.makeText(getApplicationContext(), validace, Toast.LENGTH_LONG).show();
        } else {
            switch (kategorie) {
                case "Časové pravidlo":
                    CasovePravidloRepo.updateCasovePravidlo(casovePravidlo);
                    long[] casoveUdaje = Utils.prevedTimeStringyNaMilisekundy(casovePravidlo.getCas_od(), casovePravidlo.getDoba_trvani(), false);
                    long nyni = Calendar.getInstance().getTimeInMillis();
                    ArrayList<Den> dny = casovePravidlo.getDny();
                    Den den = Utils.zjistiAktualniDen();
                    if(Utils.jeDenObsazen(dny, den) && casoveUdaje[1] > nyni) {
                        boolean zapnoutZvuky = Utils.zapnoutZvuky(getApplicationContext(), Calendar.getInstance(), 1);
                        Utils.zrusCasovace(getApplicationContext(), zapnoutZvuky);
                        Utils.nastavCasovace(getApplicationContext(), Calendar.getInstance());
                    }
                    break;
                case "Kalendářové pravidlo":

                    KalendarovePravidloRepo.updateKalendarovePravidlo(kalendarovePravidlo);
                    if(Utils.jeUdalostDnes(getApplicationContext(),kalendarovePravidlo.getUdalost())) {
                        boolean zapnoutZvuky = Utils.zapnoutZvuky(getApplicationContext(), Calendar.getInstance(), 1);
                        Utils.zrusCasovace(getApplicationContext(), zapnoutZvuky);
                        Utils.nastavCasovace(getApplicationContext(), Calendar.getInstance());
                    }
                    break;
                case "Polohové pravidlo":
                    PolohovePravidloRepo.updatePolohovePravidlo(polohovePravidlo);
                    break;
                case "Wifi pravidlo":
                    WifiPravidloRepo.updateWifiPravidlo(wifiPravidlo);
                    break;
            }
            Toast.makeText(getApplicationContext(), R.string.zmeny, Toast.LENGTH_SHORT).show();
        }
    }

    private String zvalidujVstupy() {
        String validace = "";

        if(!nazev.equals(""))
            pravidlo.setNazev(nazev);
        else
            validace += "název pravidla, ";
        pravidlo.setStav(stav);
        pravidlo.setVibrace(vibrace);

        switch (kategorie) {
            case "Časové pravidlo":
                casovePravidlo.setNazev(pravidlo.getNazev());
                casovePravidlo.setStav(pravidlo.getStav());
                casovePravidlo.setVibrace(pravidlo.getVibrace());
                if(vybraneDny.size() != 0)
                    casovePravidlo.setDny(vratDnyPravidla(vybraneDny));
                else
                    validace += "den/dny opakování, ";
                if(!cas_od.equals(""))
                    casovePravidlo.setCas_od(cas_od);
                else
                    validace += "čas od, ";
                if(!doba_trvani.equals(""))
                    casovePravidlo.setDoba_trvani(doba_trvani);
                else
                    validace += "doba trvání, ";
                break;
            case "Kalendářové pravidlo":
                kalendarovePravidlo.setNazev(pravidlo.getNazev());
                kalendarovePravidlo.setStav(pravidlo.getStav());
                kalendarovePravidlo.setVibrace(pravidlo.getVibrace());
                lze = KalendarovePravidloRepo.lzeUpdatovat(udalost, id) && KalendarovePravidloRepo.lzeNazevUdalostiPouzit(udalost, id);
                if(!udalost.equals("")) {
                    if(lze)
                        kalendarovePravidlo.setUdalost(udalost);
                } else
                    validace += "událost, ";
                break;
            case "Polohové pravidlo":
                polohovePravidlo.setNazev(pravidlo.getNazev());
                polohovePravidlo.setStav(pravidlo.getStav());
                polohovePravidlo.setVibrace(pravidlo.getVibrace());
                if(latitude != Double.MIN_VALUE & longitude != Double.MIN_VALUE) {
                    polohovePravidlo.setLatitude(latitude);
                    polohovePravidlo.setLongitude(longitude);
                } else
                    validace += "poloha, ";
                if(radius != 0)
                    polohovePravidlo.setRadius(radius);
                else
                    validace += "radius, ";
                break;
            case "Wifi pravidlo":
                wifiPravidlo.setNazev(pravidlo.getNazev());
                wifiPravidlo.setStav(pravidlo.getStav());
                wifiPravidlo.setVibrace(pravidlo.getVibrace());
                if(!nazev_wifi.equals(""))
                    wifiPravidlo.setNazev_wifi(nazev_wifi);
                else
                    validace += "název wifi, ";
                break;
        }

        return validace;
    }

    private ArrayList<Den> vratDnyPravidla(ArrayList<Integer> vybraneDny) {
        ArrayList<Integer> kolekceID = new ArrayList<>();

        for(int i = 0; i < vybraneDny.size(); i++) {
            kolekceID.add(vybraneDny.get(i) + 1);
        }

        ArrayList<Den> dnyPravidla = new ArrayList<>();
        Den den;
        for(int i = 0;i < kolekceID.size();i++) {
            den = DenRepo.getDenByID(kolekceID.get(i));
            dnyPravidla.add(den);
        }

        return dnyPravidla;
    }

    private ArrayList<String> zjistiParametryPravidla(Pravidlo pravidlo, String kategorie) {
        Field parametrySpolecne[] = pravidlo.getClass().getSuperclass().getDeclaredFields();
        Field parametryRuzne[] = pravidlo.getClass().getDeclaredFields();

        parametryVse = new ArrayList<>();
        int poziceStav = 0;
        int poziceVibrace = 0;

        for(int i=0;i<parametrySpolecne.length;i++){
            if(!parametrySpolecne[i].getName().equals("$change") & !parametrySpolecne[i].getName().equals("serialVersionUID")
                    & !parametrySpolecne[i].getName().equals("id_pravidlo") & !parametrySpolecne[i].getName().equals("kategorie")) {
                if(parametrySpolecne[i].getName().equals("stav"))
                    poziceStav = i;
                else if(parametrySpolecne[i].getName().equals("vibrace"))
                    poziceVibrace = i;
                else
                    parametryVse.add(parametrySpolecne[i].getName());
            }
        }

        if(kategorie.equals("Polohové pravidlo")) {
            for (int i = 0; i < parametryRuzne.length; i++) {
                if (!parametryRuzne[i].getName().equals("$change") & !parametryRuzne[i].getName().equals("serialVersionUID")
                        & !parametryRuzne[i].getName().equals("longitude")) {
                    if(parametryRuzne[i].getName().equals("latitude"))
                        parametryVse.add("poloha");
                    else
                        parametryVse.add(parametryRuzne[i].getName());
                }
            }
        } else if(kategorie.equals("Časové pravidlo")) {
            for (int i = 0; i < parametryRuzne.length; i++) {
                if (!parametryRuzne[i].getName().equals("$change") & !parametryRuzne[i].getName().equals("serialVersionUID")
                        & !parametryRuzne[i].getName().equals("cas_od_long") & !parametryRuzne[i].getName().equals("cas_do_long")) {
                    if(parametryRuzne[i].getName().equals("dny")) {
                        parametryVse.add(1,parametryRuzne[i].getName());
                    } else
                        parametryVse.add(parametryRuzne[i].getName());
                }
            }
        } else {
            for (int i = 0; i < parametryRuzne.length; i++) {
                if (!parametryRuzne[i].getName().equals("$change") & !parametryRuzne[i].getName().equals("serialVersionUID")) {
                    parametryVse.add(parametryRuzne[i].getName());
                }
            }
        }

        parametryVse.add(parametrySpolecne[poziceVibrace].getName());
        parametryVse.add(parametrySpolecne[poziceStav].getName());

        return parametryVse;
    }

    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (parametryVse.get(position)) {
                case LVAdapter.NAZEV:
                case LVAdapter.UDALOST:
                    zobrazEditTextDialog(view, position);
                    break;
                case LVAdapter.RADIUS:
                    zobrazRadiusDialog(view);
                    break;
                case LVAdapter.CAS_OD:
                    zobrazCasOdDialog(view);
                    break;
                case LVAdapter.DOBA_TRVANI:
                    zobrazDobaTrvaniDialog(view);
                    break;
                case LVAdapter.DNY:
                    zobrazDnyDialog(view);
                    break;
                case LVAdapter.VIBRACE:
                case LVAdapter.STAV:
                    zmenStavPrepinace(view);
                    break;
                case LVAdapter.POLOHA:
                    zjistiPolohu(view);
                    break;
                case LVAdapter.WIFI:
                    wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
                    if(wifiManager != null) {
                        if (wifiManager.isWifiEnabled() == false) {
                            wifiScanPovolen = false;
                            new WifiAsyncTask(view).execute();
                        } else {
                            zobrazWifiDialog(view);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.wifiNepodporovano, Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    };

    private void zmenStavPrepinace(View view) {
        tvSwitchNadpis = (TextView) view.findViewById(R.id.tvSwitchNadpis);
        tvSwitchObsah = (TextView) view.findViewById(R.id.tvSwitchObsah);
        prepinac = (Switch) view.findViewById(R.id.swID);

        if(tvSwitchNadpis.getText().toString().toLowerCase().equals("vibrace")) {
            if(prepinac.isChecked()) {
                prepinac.setChecked(false);
                tvSwitchObsah.setText(R.string.ne);
                vibrace = 0;
            } else {
                prepinac.setChecked(true);
                tvSwitchObsah.setText(R.string.ano);
                vibrace = 1;
            }
        } else if(tvSwitchNadpis.getText().toString().toLowerCase().equals("stav")) {
            if(prepinac.isChecked()) {
                prepinac.setChecked(false);
                tvSwitchObsah.setText(R.string.neaktiv);
                stav = 0;
            } else {
                prepinac.setChecked(true);
                tvSwitchObsah.setText(R.string.aktiv);
                stav = 1;
            }
        }
    }

    private void zobrazDnyDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        builder = new AlertDialog.Builder(DetailPravidlaActivity.this, R.style.DialogStyle);
        builder.setTitle(tvNadpis.getText());

        final boolean[] isSelectedArray = {false,false,false,false,false,false,false};
        if(!vybraneDny.isEmpty()) {
            for(int i = 0;i < vybraneDny.size();i++) {
                isSelectedArray[vybraneDny.get(i)] = true;
            }
        }

        builder.setMultiChoiceItems(DenRepo.getNazvyDnuArray(), isSelectedArray, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    vybraneDny.add(which);
                } else {
                    vybraneDny.remove(Integer.valueOf(which));
                }
            }
        });

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                        if (!vybraneDny.isEmpty()) {
                            Collections.sort(vybraneDny);
                            StringBuilder stringBuilder = new StringBuilder();

                            if (vybraneDny.size() == 7) {
                                stringBuilder = stringBuilder.append("Celý týden");
                            } else if (vybraneDny.size() == 5 & !vybraneDny.contains(5) & !vybraneDny.contains(6)) {
                                stringBuilder = stringBuilder.append("Pracovní dny");
                            } else if (vybraneDny.size() == 2 & vybraneDny.contains(5) & vybraneDny.contains(6)) {
                                stringBuilder = stringBuilder.append("Víkend");
                            } else if (vybraneDny.size() <= 4) {
                                for (int i = 0; i < vybraneDny.size(); i++) {
                                    String den = dny.get(vybraneDny.get(i)).getNazev();
                                    if(i < vybraneDny.size() - 1)
                                        stringBuilder = stringBuilder.append(den + ", ");
                                    else
                                        stringBuilder = stringBuilder.append(den);
                                }
                            } else {
                                for (int i = 0; i < vybraneDny.size(); i++) {
                                    String den = dny.get(vybraneDny.get(i)).getZkratka();
                                    if(i < vybraneDny.size() - 1)
                                        stringBuilder = stringBuilder.append(den + ", ");
                                    else
                                        stringBuilder = stringBuilder.append(den);
                                }
                            }

                            tvObsah.setText(stringBuilder.toString());

                            vybraneDnyBackup = (ArrayList<Integer>) vybraneDny.clone();
                        } else {
                            tvObsah.setText(R.string.dobaTrvaniObsah);
                        }

                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                vybraneDny = (ArrayList<Integer>) vybraneDnyBackup.clone();
                                dialogBox.cancel();
                            }
                        })

                .setNeutralButton(R.string.btnOznacitVse, null);

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
        alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
        alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vybraneDny.clear();

                for (int i = 0; i < isSelectedArray.length; i++) {
                    alertDialog.getListView().setItemChecked(i, true);
                    vybraneDny.add(i);
                }
            }
        });
    }

    private void zjistiPolohu(View view) {
        tvPoloha = (TextView) view.findViewById(R.id.tvPoloha);
        tvLat = (TextView) view.findViewById(R.id.tvLatitude);
        tvLon = (TextView) view.findViewById(R.id.tvLongitude);

        final ProgressDialog polohaProgressDialog = new ProgressDialog(DetailPravidlaActivity.this, R.style.DialogStyle);
        polohaProgressDialog.setTitle(R.string.vyckejte);
        polohaProgressDialog.setMessage("Zjišťuji polohu...");
        polohaProgressDialog.setCancelable(true);
        polohaProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        polohaProgressDialog.show();

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (isNetworkEnabled) {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if(isConnected) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                locationManager.requestSingleUpdate(criteria, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        tvLat.setText(location.getLatitude()+"");
                        tvLon.setText(location.getLongitude() + "");
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        polohaProgressDialog.dismiss();
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }
                }, null);

            } else {
                polohaProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), R.string.zadnePripojeni, Toast.LENGTH_SHORT).show();
            }
        } else {
            polohaProgressDialog.dismiss();
            Toast.makeText(getApplicationContext(), R.string.noNetworkProvider, Toast.LENGTH_SHORT).show();
        }
    }

    private void zobrazEditTextDialog(View view, final int position) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.edittext_dialog_box, null);

        builder = new AlertDialog.Builder(DetailPravidlaActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final EditText input = (EditText) mView.findViewById(R.id.etInput);
        input.setText(tvObsah.getText());
        input.setSelection(input.getText().length());

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        tvObsah.setText(input.getText().toString());
                        switch (parametryVse.get(position)) {
                            case LVAdapter.NAZEV:
                                nazev = tvObsah.getText().toString();
                                break;
                            case LVAdapter.UDALOST:
                                udalost = tvObsah.getText().toString();
                                break;
                        }
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private void zobrazRadiusDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.numberpicker_dialog_box, null);

        builder = new AlertDialog.Builder(DetailPravidlaActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final NumberPicker numberPicker = (NumberPicker) mView.findViewById(R.id.npRadius);
        numberPicker.setMaxValue(30);
        numberPicker.setMinValue(2);
        numberPicker.setWrapSelectorWheel(true);

        String[] pickerValues = new String[29];
        for (int i = 0; i < pickerValues.length; i++) {
            pickerValues[i] = (i+2)*5 + " m";
        }
        numberPicker.setDisplayedValues(pickerValues);

        String regexStr = "^[0-9]* m$";
        if(tvObsah.getText().toString().matches(regexStr))
        {
            String text = tvObsah.getText().toString();
            numberPicker.setValue(Integer.parseInt(text.substring(0, text.indexOf(" "))) / 5);
        }

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        tvObsah.setText(numberPicker.getValue() * 5 + " m");
                        radius = numberPicker.getValue() * 5;
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private void zobrazCasOdDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.mytimepicker_dialog_box, null);

        builder = new AlertDialog.Builder(DetailPravidlaActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final NumberPicker npHod = (NumberPicker) mView.findViewById(R.id.npHod);
        npHod.setMaxValue(23);
        npHod.setMinValue(0);
        npHod.setWrapSelectorWheel(true);

        final String[] pickerValuesHod = new String[24];
        for (int i = 0; i < pickerValuesHod.length; i++) {
            pickerValuesHod[i] = pad(i);
        }
        npHod.setDisplayedValues(pickerValuesHod);

        final NumberPicker npMin = (NumberPicker) mView.findViewById(R.id.npMin);
        npMin.setMaxValue(59);
        npMin.setMinValue(0);
        npMin.setWrapSelectorWheel(true);

        String[] pickerValuesMin = new String[60];
        for (int i = 0; i < pickerValuesMin.length; i++) {
            pickerValuesMin[i] = pad(i);
        }
        npMin.setDisplayedValues(pickerValuesMin);

        String regexStr = "^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$";
        if(tvObsah.getText().toString().matches(regexStr))
        {
            String text = tvObsah.getText().toString();
            npHod.setValue(Integer.parseInt(unpad(text.substring(0, text.indexOf(":")))));
            npMin.setValue(Integer.parseInt(unpad(text.substring(text.indexOf(":") + 1, text.length()))));
        }

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        tvObsah.setText(pad(npHod.getValue()) + ":" + pad(npMin.getValue()));
                        cas_od = tvObsah.getText().toString();
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private static String unpad(String c) {
        if (Integer.parseInt(c) >= 10)
            return String.valueOf(c);
        else
            return c.substring(1);
    }

    private void zobrazDobaTrvaniDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.mytimepicker_dialog_box2, null);

        builder = new AlertDialog.Builder(DetailPravidlaActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final NumberPicker npHod = (NumberPicker) mView.findViewById(R.id.npHod);
        npHod.setMaxValue(23);
        npHod.setMinValue(0);
        npHod.setWrapSelectorWheel(true);

        final String[] pickerValuesHod = new String[24];
        for (int i = 0; i < pickerValuesHod.length; i++) {
            pickerValuesHod[i] = pad(i);
        }
        npHod.setDisplayedValues(pickerValuesHod);

        final NumberPicker npMin = (NumberPicker) mView.findViewById(R.id.npMin);
        npMin.setMaxValue(59);
        npMin.setMinValue(0);
        npMin.setWrapSelectorWheel(true);

        String[] pickerValuesMin = new String[60];
        for (int i = 0; i < pickerValuesMin.length; i++) {
            pickerValuesMin[i] = pad(i);
        }
        npMin.setDisplayedValues(pickerValuesMin);

        if(npHod.getValue()==0 && npMin.getValue()==0)
            npMin.setValue(1);

        String regexStr = "^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$";
        if(tvObsah.getText().toString().matches(regexStr))
        {
            String text = tvObsah.getText().toString();
            npHod.setValue(Integer.parseInt(unpad(text.substring(0, text.indexOf(":")))));
            npMin.setValue(Integer.parseInt(unpad(text.substring(text.indexOf(":") + 1, text.length()))));
        }

        final TextView tvKonec = (TextView) mView.findViewById(R.id.tvKonec);

        npHod.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if(npHod.getValue()==0 && npMin.getValue()==0)
                    npMin.setValue(1);
                tvKonec.setText("Konec v: " + dopocitejKonec(newVal, npMin.getValue(), cas_od));
            }
        });

        npMin.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if(npHod.getValue()==0 && npMin.getValue()==0)
                    npMin.setValue(1);
                tvKonec.setText("Konec v: " + dopocitejKonec(npHod.getValue(), newVal, cas_od));
            }
        });

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        String cas = pad(npHod.getValue()) + ":" + pad(npMin.getValue());
                        tvObsah.setText(cas);
                        doba_trvani = cas;
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private String dopocitejKonec(int h, int m, String cas_od) {
        String[] castiRetezce = cas_od.split("\\:");
        int hodiny_od = Integer.parseInt(castiRetezce[0]);
        int minuty_od = Integer.parseInt(castiRetezce[1]);

        int minuty_do = (minuty_od + m) % 60;
        int hodiny_do = ((minuty_od + m)/60 + hodiny_od + h) % 24;

        return pad(hodiny_do)+":"+pad(minuty_do);
    }

    private class WifiAsyncTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog wifiProgressDialog;
        private View view;

        public WifiAsyncTask(View view) {
            this.view = view;
        }

        @Override
        protected void onPreExecute() {
            wifiProgressDialog = new ProgressDialog(DetailPravidlaActivity.this, R.style.DialogStyle);
            wifiProgressDialog.setTitle(R.string.vyckejte);
            wifiProgressDialog.setMessage("Zapínám wifi a skenuji okolí...");
            wifiProgressDialog.setCancelable(true);
            wifiProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            wifiProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            wifiManager.setWifiEnabled(true);

            while (!wifiManager.isWifiEnabled()) {
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            int pocet = 0;
            while(!wifiScanPovolen & pocet < 25) {
                if(wifiManager.getScanResults().size() == 0) {
                    try {
                        pocet++;
                        Thread.sleep(200);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    wifiScanPovolen = true;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            wifiProgressDialog.dismiss();
            zobrazWifiDialog(view);
        }
    }

    private void zobrazWifiDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.wifi_dialog_box, null);

        builder = new AlertDialog.Builder(DetailPravidlaActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final EditText input = (EditText) mView.findViewById(R.id.etInput);

        final Spinner spinner = (Spinner) mView.findViewById(R.id.spinWifi);

        List<String> nazvyWifinList = vratNazvyWifinList();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,R.layout.spinner_item,R.id.text1, nazvyWifinList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(dataAdapter);

        final RadioButton rbWifiDaU = (RadioButton) mView.findViewById(R.id.rbWifiDostAUloz);
        final RadioButton rbWifiVlastni = (RadioButton) mView.findViewById(R.id.rbWifiVlastni);

        if(nazvyWifinList.isEmpty()) {
            boolVlastnorucne = true;
            rbWifiDaU.setEnabled(false);
        }

        rbWifiDaU.setChecked(!boolVlastnorucne);
        rbWifiVlastni.setChecked(boolVlastnorucne);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.rbWifiDostAUloz) {
                    rbWifiDaU.setChecked(true);
                    rbWifiVlastni.setChecked(false);
                    spinner.setEnabled(true);
                    input.setEnabled(false);
                    boolVlastnorucne = false;
                } else if(v.getId() == R.id.rbWifiVlastni) {
                    rbWifiDaU.setChecked(false);
                    rbWifiVlastni.setChecked(true);
                    spinner.setEnabled(false);
                    input.setEnabled(true);
                    boolVlastnorucne = true;
                }
            }
        };

        rbWifiDaU.setOnClickListener(onClickListener);
        rbWifiVlastni.setOnClickListener(onClickListener);

        if(boolVlastnorucne) {
            spinner.setEnabled(false);
            input.setEnabled(true);

            String prvniSlovo;
            try {
                prvniSlovo = tvObsah.getText().toString().substring(0,tvObsah.getText().toString().indexOf(" "));
            } catch (Exception e) {
                prvniSlovo = "";
            }

            if(prvniSlovo.equals("Vyplňte")){
                input.setHint(R.string.wifiObsah);
            } else {
                input.setText(tvObsah.getText());
                input.setSelection(input.getText().length());
            }
        } else {
            spinner.setEnabled(true);
            input.setEnabled(false);
            input.setHint(R.string.wifiObsah);

            String vyplnena_wifi = tvObsah.getText().toString();
            if(!vyplnena_wifi.equals(R.string.wifiObsah) & nazvyWifinList.contains(vyplnena_wifi))
                spinner.setSelection(nazvyWifinList.indexOf(vyplnena_wifi));
        }

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        if (boolVlastnorucne) {
                            if (input.getText().toString().length() != 0) {
                                tvObsah.setText(input.getText().toString());
                                nazev_wifi = tvObsah.getText().toString();
                            } else {
                                tvObsah.setText(R.string.wifiObsah);
                                nazev_wifi = "";
                            }
                        } else {
                            tvObsah.setText(spinner.getSelectedItem().toString());
                            nazev_wifi = tvObsah.getText().toString();
                        }
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private List<String> vratNazvyWifinList() {
        List<String> vyslednaKolekce = new ArrayList<>();

        List<ScanResult> listScan = wifiManager.getScanResults();
        if(!listScan.isEmpty()) {
            List<String> dostupne = vratListSSIDStringu(listScan);

            for(String nazev : dostupne) {
                vyslednaKolekce.add(nazev);
            }
        }

        List<WifiConfiguration> listConfig = wifiManager.getConfiguredNetworks();
        if(!listConfig.isEmpty()) {

            List<String> ulozene = vratListSSIDStringu(listConfig);

            for (String nazev : ulozene) {
                if (!vyslednaKolekce.contains(nazev)) {
                    vyslednaKolekce.add(nazev);
                }
            }
        }

        return vyslednaKolekce;
    }

    private List<String> vratListSSIDStringu(List<?> list) {
        List<String> vyslednaKolekce = new ArrayList<>();

        if(zjistiTridu(list).equals(ScanResult.class.toString())) {
            for(int i = 0; i < list.size();i++) {
                vyslednaKolekce.add(((ScanResult)list.get(i)).SSID);
            }
        } else if (zjistiTridu(list).equals(WifiConfiguration.class.toString())) {
            String s;
            for(int i =0; i < list.size();i++) {
                s = ((WifiConfiguration)list.get(i)).SSID;
                if(s.startsWith("\"") & s.endsWith("\"")) {
                    s = s.substring(1,s.length() - 1);
                }
                vyslednaKolekce.add(s);
            }
        }

        Collections.sort(vyslednaKolekce, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });

        return vyslednaKolekce;
    }

    private String zjistiTridu(List<?> list) {
        return list.get(0).getClass().toString();
    }
}
