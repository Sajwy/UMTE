package cz.sajwy.testapp.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import cz.sajwy.testapp.activities.others.Utils;
import cz.sajwy.testapp.data.repo.CasovaceRepo;
import cz.sajwy.testapp.data.repo.ConfigRepo;
import cz.sajwy.testapp.service.ObsluhaPravidelService;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(CasovaceRepo.get() == 1) {
            Utils.zrusCasovace(context, false);
            Utils.nastavCasovace(context, Calendar.getInstance());
        }
    }
}
