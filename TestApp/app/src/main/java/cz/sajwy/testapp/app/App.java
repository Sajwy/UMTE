package cz.sajwy.testapp.app;

import android.app.Application;
import android.content.Context;

import cz.sajwy.testapp.data.DBHelper;
import cz.sajwy.testapp.data.DBManager;

public class App extends Application {
    private static Context context;
    private static DBHelper dbHelper;

    @Override
    public void onCreate()
    {
        super.onCreate();
        context = this.getApplicationContext();
        dbHelper = new DBHelper();
        DBManager.initializeInstance(dbHelper);
    }

    public static Context getContext(){
        return context;
    }
}
