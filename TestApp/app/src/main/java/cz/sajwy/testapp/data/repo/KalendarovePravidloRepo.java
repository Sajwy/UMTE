package cz.sajwy.testapp.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import cz.sajwy.testapp.data.DBManager;
import cz.sajwy.testapp.data.model.KalendarovePravidlo;
import cz.sajwy.testapp.data.model.Pravidlo;

public class KalendarovePravidloRepo {
    private static final String TABLE = "KalendarovePravidlo";

    private static final String KEY_FK_PRAVIDLO = "ID_pravidlo";
    private static final String KEY_UDALOST = "Udalost";

    private static final String[] COLUMNS = {KEY_FK_PRAVIDLO, KEY_UDALOST};

    public KalendarovePravidloRepo() {
    }

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_FK_PRAVIDLO  + " INTEGER REFERENCES Pravidlo," +
                    KEY_UDALOST + " TEXT UNIQUE " +
                ");";
    }

    public static String createIndex() {
        return "CREATE INDEX ix_id_kp ON " + TABLE + "(" + KEY_FK_PRAVIDLO + ");"+
                "CREATE INDEX ix_udalost ON " + TABLE + "(" + KEY_UDALOST + ");";
    }

    public static ArrayList<String> getNazvyAktivnichUdalosti() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT " + KEY_UDALOST + " FROM " + TABLE + " kp JOIN Pravidlo p ON kp." + KEY_FK_PRAVIDLO + " = p.ID_pravidlo " +
                        "WHERE p.Stav = 1";
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<String> udalosti = new ArrayList<>();

        while (cursor.moveToNext()) {
            udalosti.add(cursor.getString(cursor.getColumnIndex(KEY_UDALOST)));
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return udalosti;
    }

    public static int getVibrace(String udalost) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT Vibrace FROM " + TABLE + " kp JOIN Pravidlo p ON kp." + KEY_FK_PRAVIDLO + " = p.ID_pravidlo " +
                        "WHERE " + KEY_UDALOST + " = '" + udalost + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null)
            cursor.moveToFirst();

        int vibrace = cursor.getInt(cursor.getColumnIndex("Vibrace"));
        cursor.close();
        DBManager.getInstance().closeDatabase();
        return vibrace;
    }

    public static KalendarovePravidlo getKalendarovePravidlo(int id) {
        Pravidlo pravidlo = PravidloRepo.getPravidlo(id);

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        Cursor cursor = db.query(TABLE, // a. table
                COLUMNS, // b. column names
                KEY_FK_PRAVIDLO + " = ?", // c. selections
                new String[]{String.valueOf(id)}, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        KalendarovePravidlo kalendarovePravidlo = new KalendarovePravidlo(id,pravidlo.getNazev(),pravidlo.getStav(),
                pravidlo.getVibrace(),pravidlo.getKategorie(),cursor.getString(cursor.getColumnIndex(KEY_UDALOST)));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return kalendarovePravidlo;
    }

    public static void insertKalendarovePravidlo(KalendarovePravidlo kalendarovePravidlo){
        int id = PravidloRepo.insertPravidlo(new Pravidlo(kalendarovePravidlo.getNazev(), kalendarovePravidlo.getStav(),
                kalendarovePravidlo.getVibrace(), kalendarovePravidlo.getKategorie()));

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "INSERT INTO " + TABLE + " (" + KEY_FK_PRAVIDLO + ", " + KEY_UDALOST + ") " +
                        "VALUES " + "(" + id + " ,'" + kalendarovePravidlo.getUdalost() + "');";

        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static void updateKalendarovePravidlo(KalendarovePravidlo novePravidlo) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "UPDATE " + TABLE + " SET " + KEY_UDALOST + " = '" + novePravidlo.getUdalost() +
                        "' WHERE " + KEY_FK_PRAVIDLO + " = " + novePravidlo.getId_pravidlo();
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        Pravidlo pravidlo = new Pravidlo(novePravidlo.getId_pravidlo(), novePravidlo.getNazev(), novePravidlo.getStav(),
                                            novePravidlo.getVibrace(),novePravidlo.getKategorie());
        PravidloRepo.updatePravidlo(pravidlo);
    }

    public static void deleteKalendarovePravidlo(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "DELETE FROM " + TABLE + " WHERE " + KEY_FK_PRAVIDLO + " = " + id;
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        PravidloRepo.deletePravidlo(id);
    }

    public static boolean lzeNazevUdalostiPouzit(String nazev) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        String query = "SELECT * FROM " + TABLE + " WHERE lower(" + KEY_UDALOST + ") = '" + nazev.toLowerCase() + "'";
        Cursor cursor = db.rawQuery(query, null);
        boolean lze = true;
        if (cursor != null) {
            if (cursor.moveToFirst())
                lze = false;
            else
                lze = true;
        }
        cursor.close();
        DBManager.getInstance().closeDatabase();
        return lze;
    }

    public static boolean lzeNazevUdalostiPouzit(String nazev, int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        String query = "SELECT * FROM " + TABLE + " WHERE lower(" + KEY_UDALOST + ") = '" + nazev.toLowerCase() + "' AND " +
                        KEY_FK_PRAVIDLO + " <> " + id;
        Cursor cursor = db.rawQuery(query, null);
        boolean lze = true;
        if (cursor != null) {
            if (cursor.moveToFirst())
                lze = false;
            else
                lze = true;
        }
        cursor.close();
        DBManager.getInstance().closeDatabase();
        return lze;
    }

    public static boolean lzeUpdatovat(String nazev, int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        String query = "SELECT * FROM " + TABLE +
                " WHERE (lower(" + KEY_UDALOST + ") = '" + nazev.toLowerCase() + "' AND " + KEY_FK_PRAVIDLO + " = " + id + ")" +
                " OR (" + KEY_FK_PRAVIDLO + " = " + id + ")";
        Cursor cursor = db.rawQuery(query, null);
        boolean lze = false;
        if (cursor != null) {
            if (cursor.moveToFirst())
                lze = true;
            else
                lze = false;
        }
        cursor.close();
        DBManager.getInstance().closeDatabase();
        return lze;
    }
}
