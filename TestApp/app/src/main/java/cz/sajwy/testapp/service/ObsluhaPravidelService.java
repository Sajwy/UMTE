package cz.sajwy.testapp.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import cz.sajwy.testapp.data.repo.ConfigRepo;

public class ObsluhaPravidelService extends Service {
    AudioManager audioManager;
    String rezim;

    public ObsluhaPravidelService() {}

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        rezim = intent.getStringExtra("rezim");
        switch (rezim) {
            case "vibrace":
                audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                ConfigRepo.update(1);
                break;
            case "ticho":
                audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                ConfigRepo.update(1);
                break;
            case "normal":
                audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                ConfigRepo.update(0);
                break;
        }
        //stopSelf(); ???
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}