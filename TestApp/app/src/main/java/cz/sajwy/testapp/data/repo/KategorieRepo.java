package cz.sajwy.testapp.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import cz.sajwy.testapp.data.DBManager;
import cz.sajwy.testapp.data.model.Kategorie;

public class KategorieRepo {
    private static final String TABLE = "Kategorie";

    private static final String KEY_ID_KATEGORIE = "ID_kategorie";
    private static final String KEY_NAZEV = "Nazev";

    private static final String[] COLUMNS = {KEY_ID_KATEGORIE,KEY_NAZEV};

    public KategorieRepo() {
    }

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                KEY_ID_KATEGORIE  + " INTEGER PRIMARY KEY," +
                KEY_NAZEV + " TEXT " +
                ");";
    }

    public static String createIndex() {
        return "CREATE INDEX ix_id_kategorie ON " + TABLE + "(" + KEY_ID_KATEGORIE + ");"+
                "CREATE INDEX ix_nazev_kategorie ON " + TABLE + "(" + KEY_NAZEV + ");";
    }

    public static String insertData(){
        return  "INSERT INTO " + TABLE + " (" + KEY_NAZEV + ") VALUES " +
                "('Časové pravidlo'), " +
                "('Kalendářové pravidlo'), " +
                "('Polohové pravidlo'), " +
                "('Wifi pravidlo');";
                /*+
                "('Google places pravidlo'), " +
                "('Sloučené pravidlo'); ";*/
    }

    public static Kategorie getKategorieByID(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        Cursor cursor =
                db.query(TABLE, // a. table
                        COLUMNS, // b. column names
                        KEY_ID_KATEGORIE + " = ?", // c. selections
                        new String[]{String.valueOf(id)}, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        Kategorie kategorie = new Kategorie(id, cursor.getString(cursor.getColumnIndex(KEY_NAZEV)));
        cursor.close();
        DBManager.getInstance().closeDatabase();

        return kategorie;
    }

    public static Kategorie getKategorieByIDPravidla(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT FK_kategorie FROM Pravidlo WHERE ID_pravidlo = " + id;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null)
            cursor.moveToFirst();

        int idKategorie = cursor.getInt(cursor.getColumnIndex("FK_kategorie"));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        Kategorie kategorie = getKategorieByID(idKategorie);

        return kategorie;
    }

    public static Kategorie getKategorieByNazev(String nazev) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        Cursor cursor =
                db.query(TABLE, // a. table
                        COLUMNS, // b. column names
                        KEY_NAZEV + " = ?", // c. selections
                        new String[]{String.valueOf(nazev)}, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        Kategorie kategorie = new Kategorie(cursor.getInt(cursor.getColumnIndex(KEY_ID_KATEGORIE)), cursor.getString(cursor.getColumnIndex(KEY_NAZEV)));
        cursor.close();
        DBManager.getInstance().closeDatabase();

        return kategorie;
    }

    public static ArrayList<Kategorie> getAllKategorie() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT * FROM " + TABLE;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Kategorie> kategorie = new ArrayList<Kategorie>();

        if (cursor.moveToFirst()) {
            do {
                Kategorie k = new Kategorie(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID_KATEGORIE))),
                        cursor.getString(cursor.getColumnIndex(KEY_NAZEV)));

                kategorie.add(k);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return kategorie;
    }

    public static ArrayList<String> getAllKategorieNazvy() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT * FROM " + TABLE;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<String> kategorieNazvy = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                kategorieNazvy.add(cursor.getString(cursor.getColumnIndex(KEY_NAZEV)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return kategorieNazvy;
    }

    public static ArrayList<Kategorie> getKategorieSPravidly() {
        ArrayList<Kategorie> kategorie = getAllKategorie();

        for(int i = 0;i < kategorie.size();i++) {
            Kategorie k = kategorie.get(i);
            k.setPravidla(PravidloRepo.getPravidlaByKategorie(k.getId_kategorie()));
        }

        return kategorie;
    }
}
