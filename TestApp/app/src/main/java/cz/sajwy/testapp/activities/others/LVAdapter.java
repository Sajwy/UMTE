package cz.sajwy.testapp.activities.others;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import cz.sajwy.testapp.R;
import cz.sajwy.testapp.data.model.CasovePravidlo;
import cz.sajwy.testapp.data.model.Den;
import cz.sajwy.testapp.data.model.KalendarovePravidlo;
import cz.sajwy.testapp.data.model.PolohovePravidlo;
import cz.sajwy.testapp.data.model.Pravidlo;
import cz.sajwy.testapp.data.model.WifiPravidlo;
import cz.sajwy.testapp.data.repo.CasovePravidloRepo;
import cz.sajwy.testapp.data.repo.DenRepo;
import cz.sajwy.testapp.data.repo.KalendarovePravidloRepo;
import cz.sajwy.testapp.data.repo.KategorieRepo;
import cz.sajwy.testapp.data.repo.PolohovePravidloRepo;
import cz.sajwy.testapp.data.repo.PravidloRepo;
import cz.sajwy.testapp.data.repo.WifiPravidloRepo;

public class LVAdapter extends ArrayAdapter {
    //KONSTANTY pro atributy
    //pravidlo
    public static final String NAZEV = "nazev";
    public static final String STAV = "stav";
    public static final String VIBRACE = "vibrace";

    //casove pravidlo
    public static final String DNY = "dny";
    public static final String CAS_OD = "cas_od";
    public static final String DOBA_TRVANI = "doba_trvani";

    //kalendarove pravidlo
    public static final String UDALOST = "udalost";

    //polohove pravidlo
    public static final String POLOHA = "poloha";
    public static final String RADIUS = "radius";

    //wifi pravidlo
    public static final String WIFI = "nazev_wifi";

    private Context context;
    private int resource;
    private ArrayList<String> parametry = null;
    private int idPravidla;
    TextView tvNadpis;
    TextView tvObsah;
    TextView tvSwitchNadpis;
    TextView tvSwitchObsah;
    TextView tvLat;
    TextView tvLon;
    TextView tvPoloha;
    Switch prepinac;
    Pravidlo pravidlo;
    CasovePravidlo casovePravidlo;
    KalendarovePravidlo kalendarovePravidlo;
    WifiPravidlo wifiPravidlo;
    PolohovePravidlo polohovePravidlo;

    public LVAdapter(Context context, int resource, ArrayList<String> parametry, int idPravidla) {
        super(context, resource, parametry);
        this.context = context;
        this.resource = resource;
        this.parametry = parametry;
        this.idPravidla = idPravidla;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            switch (getItemViewType(position)) {
                case 0:
                    convertView = LayoutInflater.from(context).inflate(R.layout.lv_nove_pravidlo_tv, parent, false);

                    tvNadpis = (TextView) convertView.findViewById(R.id.tvNadpis);
                    tvObsah = (TextView) convertView.findViewById(R.id.tvObsah);
                    break;
                case 1:
                    convertView = LayoutInflater.from(context).inflate(R.layout.lv_nove_pravidlo_switch, parent, false);

                    tvSwitchNadpis = (TextView) convertView.findViewById(R.id.tvSwitchNadpis);
                    tvSwitchObsah = (TextView) convertView.findViewById(R.id.tvSwitchObsah);
                    prepinac = (Switch) convertView.findViewById(R.id.swID);
                    break;
                case 2:
                    convertView = LayoutInflater.from(context).inflate(R.layout.lv_nove_pravidlo_poloha, parent, false);

                    tvPoloha = (TextView) convertView.findViewById(R.id.tvPoloha);
                    tvLat = (TextView) convertView.findViewById(R.id.tvLatitude);
                    tvLon = (TextView) convertView.findViewById(R.id.tvLongitude);
                    break;
            }

            if(idPravidla > 0) {
                pravidlo = PravidloRepo.getPravidlo(idPravidla);

                switch (KategorieRepo.getKategorieByIDPravidla(idPravidla).getNazev()) {
                    case "Časové pravidlo":
                        casovePravidlo = CasovePravidloRepo.getCasovePravidlo(idPravidla);
                        break;
                    case "Kalendářové pravidlo":
                        kalendarovePravidlo = KalendarovePravidloRepo.getKalendarovePravidlo(idPravidla);
                        break;
                    case "Polohové pravidlo":
                        polohovePravidlo = PolohovePravidloRepo.getPolohovePravidlo(idPravidla);
                        break;
                    case "Wifi pravidlo":
                        wifiPravidlo = WifiPravidloRepo.getWifiPravidlo(idPravidla);
                        break;
                }
            }

            String parametr = parametry.get(position);

            switch (parametr) {
                case NAZEV:
                    tvNadpis.setText(R.string.nazevNadpis);
                    if(idPravidla == 0)
                        tvObsah.setText(R.string.nazevObsah);
                    else
                        tvObsah.setText(pravidlo.getNazev());
                    break;
                case VIBRACE:
                    tvSwitchNadpis.setText(R.string.vibraceNadpis);
                    if(idPravidla == 0) {
                        tvSwitchObsah.setText(R.string.ano);
                        prepinac.setChecked(true);
                    } else {
                        if(pravidlo.getVibrace() == 1) {
                            tvSwitchObsah.setText(R.string.ano);
                            prepinac.setChecked(true);
                        } else {
                            tvSwitchObsah.setText(R.string.ne);
                            prepinac.setChecked(false);
                        }
                    }
                    break;
                case STAV:
                    tvSwitchNadpis.setText(R.string.stavNadpis);
                    if(idPravidla == 0) {
                        tvSwitchObsah.setText(R.string.aktiv);
                        prepinac.setChecked(true);
                    } else {
                        if(pravidlo.getStav() == 1) {
                            tvSwitchObsah.setText(R.string.aktiv);
                            prepinac.setChecked(true);
                        } else {
                            tvSwitchObsah.setText(R.string.neaktiv);
                            prepinac.setChecked(false);
                        }
                    }
                    break;
                case DNY:
                    tvNadpis.setText(R.string.denNadpis);
                    if(idPravidla == 0)
                        tvObsah.setText(R.string.denObsah);
                    else {
                        ArrayList<Den> dny = DenRepo.getDny();
                        ArrayList<Integer> vybraneDny = CasovePravidloRepo.getIntListDnuCasovehoPravidla(casovePravidlo.getId_pravidlo());
                        Collections.sort(vybraneDny);
                        StringBuilder stringBuilder = new StringBuilder();

                        if (vybraneDny.size() == 7) {
                            stringBuilder = stringBuilder.append("Celý týden");
                        } else if (vybraneDny.size() == 5 & !vybraneDny.contains(5) & !vybraneDny.contains(6)) {
                            stringBuilder = stringBuilder.append("Pracovní dny");
                        } else if (vybraneDny.size() == 2 & vybraneDny.contains(5) & vybraneDny.contains(6)) {
                            stringBuilder = stringBuilder.append("Víkend");
                        } else if (vybraneDny.size() <= 4) {
                            for (int i = 0; i < vybraneDny.size(); i++) {
                                String den = dny.get(vybraneDny.get(i)).getNazev();
                                if (i < vybraneDny.size() - 1)
                                    stringBuilder = stringBuilder.append(den + ", ");
                                else
                                    stringBuilder = stringBuilder.append(den);
                            }
                        } else {
                            for (int i = 0; i < vybraneDny.size(); i++) {
                                String den = dny.get(vybraneDny.get(i)).getZkratka();
                                if (i < vybraneDny.size() - 1)
                                    stringBuilder = stringBuilder.append(den + ", ");
                                else
                                    stringBuilder = stringBuilder.append(den);
                            }
                        }

                        tvObsah.setText(stringBuilder.toString());
                    }
                    break;
                case CAS_OD:
                    tvNadpis.setText(R.string.casOdNadpis);
                    if(idPravidla == 0)
                        tvObsah.setText(R.string.casOdObsah);
                    else
                        tvObsah.setText(casovePravidlo.getCas_od());
                    break;
                case DOBA_TRVANI:
                    tvNadpis.setText(R.string.dobaTrvaniNadpis);
                    if(idPravidla == 0)
                        tvObsah.setText(R.string.dobaTrvaniObsah);
                    else
                        tvObsah.setText(casovePravidlo.getDoba_trvani());
                    break;
                case UDALOST:
                    tvNadpis.setText(R.string.udalostNadpis);
                    if(idPravidla == 0)
                        tvObsah.setText(R.string.udalostObsah);
                    else
                        tvObsah.setText(kalendarovePravidlo.getUdalost());
                    break;
                case POLOHA:
                    tvPoloha.setText(R.string.polohaNadpis);
                    if(idPravidla == 0) {
                        tvLat.setText(R.string.polohaObsah);
                        tvLon.setText(R.string.polohaObsah);
                    } else {
                        tvLat.setText(polohovePravidlo.getLatitude()+"");
                        tvLon.setText(polohovePravidlo.getLongitude()+"");
                    }
                    break;
                case RADIUS:
                    tvNadpis.setText(R.string.radiusNadpis);
                    if(idPravidla == 0)
                        tvObsah.setText(R.string.radiusObsah);
                    else
                        tvObsah.setText(polohovePravidlo.getRadius()+" m");
                    break;
                case WIFI:
                    tvNadpis.setText(R.string.wifiNadpis);
                    if(idPravidla == 0)
                        tvObsah.setText(R.string.wifiObsah);
                    else
                        tvObsah.setText(wifiPravidlo.getNazev_wifi());
                    break;
            }
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        String parametr = parametry.get(position);
        int navratovaHodnota = 0;

        switch (parametr) {
            case NAZEV:
                navratovaHodnota = 0;
                break;
            case STAV:
                navratovaHodnota = 1;
                break;
            case VIBRACE:
                navratovaHodnota = 1;
                break;
            case DNY:
                navratovaHodnota = 0;
                break;
            case CAS_OD:
                navratovaHodnota = 0;
                break;
            case DOBA_TRVANI:
                navratovaHodnota = 0;
                break;
            case UDALOST:
                navratovaHodnota = 0;
                break;
            case POLOHA:
                navratovaHodnota = 2;
                break;
            case RADIUS:
                navratovaHodnota = 0;
                break;
            case WIFI:
                navratovaHodnota = 0;
                break;
        }

        return navratovaHodnota;
    }
}
