package cz.sajwy.testapp.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.widget.Toast;

import java.util.Calendar;

import cz.sajwy.testapp.activities.others.Utils;
import cz.sajwy.testapp.data.repo.ConfigRepo;
import cz.sajwy.testapp.data.repo.IntentRepo;
import cz.sajwy.testapp.service.ObsluhaPravidelService;

/**
 * Created by Sajwy on 05.06.2017.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = intent.getIntExtra("id",-1);
        if(id != -1) {
            if(id == 0) {
                boolean zapnoutZvuky = Utils.zapnoutZvuky(context,Calendar.getInstance(),1);
                Utils.zrusCasovace(context, zapnoutZvuky);
                Utils.nastavCasovace(context, Calendar.getInstance());
            } else {
                String rezim = intent.getStringExtra("rezim");
                if(ConfigRepo.get() == 1 && !rezim.equals("normal")) {
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    Intent iCancel = new Intent(context, AlarmReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context,id - 1,iCancel,PendingIntent.FLAG_ONE_SHOT);
                    pendingIntent.cancel();
                    alarmManager.cancel(pendingIntent);
                    IntentRepo.delete(id - 1);
                    IntentRepo.delete(id);
                    Intent i = new Intent(context, ObsluhaPravidelService.class);
                    i.putExtra("rezim", rezim);
                    context.startService(i);
                } else {
                    IntentRepo.delete(id);
                    Intent ii = new Intent(context, ObsluhaPravidelService.class);
                    ii.putExtra("rezim", rezim);
                    context.startService(ii);
                }
            }
        }
    }
}
