package cz.sajwy.testapp.data.model;

public class PolohovePravidlo extends Pravidlo {
    private double latitude;
    private double longitude;
    private int radius;

    public PolohovePravidlo() {
    }

    public PolohovePravidlo(int id_pravidlo, String nazev, int stav, int vibrace, Kategorie kategorie, double latitude, double longitude, int radius) {
        setId_pravidlo(id_pravidlo);
        setNazev(nazev);
        setVibrace(vibrace);
        setStav(stav);
        setKategorie(kategorie);
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    public PolohovePravidlo(String nazev, int stav, int vibrace, Kategorie kategorie, double latitude, double longitude, int radius) {
        setNazev(nazev);
        setVibrace(vibrace);
        setStav(stav);
        setKategorie(kategorie);
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
