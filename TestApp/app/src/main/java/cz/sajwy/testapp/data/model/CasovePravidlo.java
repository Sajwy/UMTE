package cz.sajwy.testapp.data.model;

import java.util.ArrayList;
import java.util.List;

public class CasovePravidlo extends Pravidlo {
    private ArrayList<Den> dny;
    private String cas_od;
    private String doba_trvani;
    private long cas_od_long;
    private long cas_do_long;

    public CasovePravidlo() {
    }

    public CasovePravidlo(int id_pravidlo, String nazev, int stav, int vibrace, Kategorie kategorie, ArrayList<Den> dny, String cas_od, String doba_trvani) {
        setId_pravidlo(id_pravidlo);
        setNazev(nazev);
        setVibrace(vibrace);
        setStav(stav);
        setKategorie(kategorie);
        this.dny = dny;
        this.cas_od = cas_od;
        this.doba_trvani = doba_trvani;
    }

    public CasovePravidlo(String nazev, int stav, int vibrace, Kategorie kategorie, ArrayList<Den> dny, String cas_od, String doba_trvani) {
        setNazev(nazev);
        setVibrace(vibrace);
        setStav(stav);
        setKategorie(kategorie);
        this.dny = dny;
        this.cas_od = cas_od;
        this.doba_trvani = doba_trvani;
    }

    public ArrayList<Den> getDny() {
        return dny;
    }

    public void setDny(ArrayList<Den> dny) {
        this.dny = dny;
    }

    public String getCas_od() {
        return cas_od;
    }

    public void setCas_od(String cas_od) {
        this.cas_od = cas_od;
    }

    public String getDoba_trvani() {
        return doba_trvani;
    }

    public void setDoba_trvani(String doba_trvani) {
        this.doba_trvani = doba_trvani;
    }

    public CasovePravidlo(int id_pravidlo, String nazev, int stav, int vibrace, Kategorie kategorie, ArrayList<Den> dny, long cas_od_long, long cas_do_long) {
        setId_pravidlo(id_pravidlo);
        setNazev(nazev);
        setVibrace(vibrace);
        setStav(stav);
        setKategorie(kategorie);
        this.dny = dny;
        this.cas_od_long = cas_od_long;
        this.cas_do_long = cas_do_long;
    }

    public CasovePravidlo(String nazev, int stav, int vibrace, Kategorie kategorie, ArrayList<Den> dny, long cas_od_long, long cas_do_long) {
        setNazev(nazev);
        setVibrace(vibrace);
        setStav(stav);
        setKategorie(kategorie);
        this.dny = dny;
        this.cas_od_long = cas_od_long;
        this.cas_do_long = cas_do_long;
    }

    public long getCas_do_long() {
        return cas_do_long;
    }

    public void setCas_do_long(long cas_do_long) {
        this.cas_do_long = cas_do_long;
    }

    public long getCas_od_long() {
        return cas_od_long;
    }

    public void setCas_od_long(long cas_od_long) {
        this.cas_od_long = cas_od_long;
    }
}
